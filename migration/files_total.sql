# tables with reference to b_file (with referencing field name)
# +-------------------------------+-------------+
# | table_name                    | column_name |
# +-------------------------------+-------------+
# | alien_doc                     | FILES_ID    |
# | b_posting_file                | FILE_ID     |
# | b_ticket_message_2_file       | FILE_ID     |
# | olsc_agreement                | FILE_ID     |
# | olsc_doc                      | FILES_ID    |
# | olsc_offer                    | FILE_ID     |
# | olsc_offer_edition            | FILE_ID     |
# | olsc_offer_org                | FILE_ID     |
# | olsc_org_subordinates_tickets | FILE_ID     |
# | plan_doc                      | FILE_ID     |
# | production_production_file    | FILE_ID     |
# | tender_announce_doc           | FILE_ID     |
# | tender_contract_doc           | FILE_ID     |
# | tender_dissent_doc            | FILE_ID     |
# | tender_easuz_doc              | FILE_ID     |
# | tender_explanation_doc        | FILE_ID     |
# | tender_lot_doc                | FILE_ID     |
# | tender_offer_correction_doc   | FILE_ID     |
# | tender_offer_doc              | FILE_ID     |
# | tender_protocol_doc           | FILE_ID     |
# +-------------------------------+-------------+

SELECT COUNT(*)
FROM (
       SELECT FILE_ID
       FROM tender_protocol_doc tpd
         JOIN b_file bf
           ON tpd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_offer_doc tod
         JOIN b_file bf
           ON tod.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_offer_correction_doc tocd
         JOIN b_file bf
           ON tocd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_lot_doc tld
         JOIN b_file bf
           ON tld.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_explanation_doc ted
         JOIN b_file bf
           ON ted.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_easuz_doc tesd
         JOIN b_file bf
           ON tesd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_contract_doc tcd
         JOIN b_file bf
           ON tcd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_announce_doc tad
         JOIN b_file bf
           ON tad.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_dissent_doc tdd
         JOIN b_file bf
           ON tdd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM plan_doc pd
         JOIN b_file bf
           ON pd.FILE_ID = bf.ID
     ) AS fds;

# total tender: 278 973

SELECT COUNT(*)
FROM (
       SELECT FILE_ID
       FROM tender_protocol_doc_log tpdl
         JOIN b_file bf
           ON tpdl.FILE_ID = bf.ID
       WHERE tpdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_offer_doc_log todl
         JOIN b_file bf
           ON todl.FILE_ID = bf.ID
       WHERE todl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_offer_correction_doc_log tocdl
         JOIN b_file bf
           ON tocdl.FILE_ID = bf.ID
       WHERE tocdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_lot_doc_log tldl
         JOIN b_file bf
           ON tldl.FILE_ID = bf.ID
       WHERE tldl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_explanation_doc_log tedl
         JOIN b_file bf
           ON tedl.FILE_ID = bf.ID
       WHERE tedl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_contract_doc_log tcdl
         JOIN b_file bf
           ON tcdl.FILE_ID = bf.ID
       WHERE tcdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_announce_doc_log tadl
         JOIN b_file bf
           ON tadl.FILE_ID = bf.ID
       WHERE tadl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_dissent_doc_log tddl
         JOIN b_file bf
           ON tddl.FILE_ID = bf.ID
       WHERE tddl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM plan_doc_log pdl
         JOIN b_file bf
           ON pdl.FILE_ID = bf.ID
       WHERE pdl.`__DELETED` = 1
     ) AS fds;

# total deleted in tender: 48 626

SELECT COUNT(*)
FROM (
       SELECT FILE_ID
       FROM olsc_org_subordinates_tickets oost
         JOIN b_file bf
           ON oost.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer_org ooo
         JOIN b_file bf
           ON ooo.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer_edition ooe
         JOIN b_file bf
           ON ooe.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer oo
         JOIN b_file bf
           ON oo.FILE_ID = bf.ID

       UNION

       SELECT FILES_ID
       FROM olsc_doc od
         JOIN b_file bf
           ON od.FILES_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_agreement oa
         JOIN b_file bf
           ON oa.FILE_ID = bf.ID
     ) AS fds;

# total olsc: 23 344

SELECT COUNT(*)
FROM (
       SELECT FILE_ID
       FROM production_production_file ppf
         JOIN b_file bf
           ON ppf.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM b_ticket_message_2_file btm2f
         JOIN b_file bf
           ON btm2f.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM b_posting_file bpf
         JOIN b_file bf
           ON bpf.FILE_ID = bf.ID

       UNION

       SELECT FILES_ID
       FROM alien_doc ad
         JOIN b_file bf
           ON ad.FILES_ID = bf.ID
     ) AS fds;

# total other: 749

SELECT COUNT(*)
FROM (
       # tender

       SELECT FILE_ID
       FROM tender_protocol_doc tpd
         JOIN b_file bf
           ON tpd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_offer_doc tod
         JOIN b_file bf
           ON tod.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_offer_correction_doc tocd
         JOIN b_file bf
           ON tocd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_lot_doc tld
         JOIN b_file bf
           ON tld.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_explanation_doc ted
         JOIN b_file bf
           ON ted.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_easuz_doc tesd
         JOIN b_file bf
           ON tesd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_contract_doc tcd
         JOIN b_file bf
           ON tcd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_announce_doc tad
         JOIN b_file bf
           ON tad.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM tender_dissent_doc tdd
         JOIN b_file bf
           ON tdd.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM plan_doc pd
         JOIN b_file bf
           ON pd.FILE_ID = bf.ID

       UNION

       # tender deleted

       SELECT FILE_ID
       FROM tender_protocol_doc_log tpdl
         JOIN b_file bf
           ON tpdl.FILE_ID = bf.ID
       WHERE tpdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_offer_doc_log todl
         JOIN b_file bf
           ON todl.FILE_ID = bf.ID
       WHERE todl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_offer_correction_doc_log tocdl
         JOIN b_file bf
           ON tocdl.FILE_ID = bf.ID
       WHERE tocdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_lot_doc_log tldl
         JOIN b_file bf
           ON tldl.FILE_ID = bf.ID
       WHERE tldl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_explanation_doc_log tedl
         JOIN b_file bf
           ON tedl.FILE_ID = bf.ID
       WHERE tedl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_contract_doc_log tcdl
         JOIN b_file bf
           ON tcdl.FILE_ID = bf.ID
       WHERE tcdl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_announce_doc_log tadl
         JOIN b_file bf
           ON tadl.FILE_ID = bf.ID
       WHERE tadl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM tender_dissent_doc_log tddl
         JOIN b_file bf
           ON tddl.FILE_ID = bf.ID
       WHERE tddl.`__DELETED` = 1

       UNION

       SELECT FILE_ID
       FROM plan_doc_log pdl
         JOIN b_file bf
           ON pdl.FILE_ID = bf.ID
       WHERE pdl.`__DELETED` = 1

       UNION

       # olsc

       SELECT FILE_ID
       FROM olsc_org_subordinates_tickets oost
         JOIN b_file bf
           ON oost.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer_org ooo
         JOIN b_file bf
           ON ooo.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer_edition ooe
         JOIN b_file bf
           ON ooe.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_offer oo
         JOIN b_file bf
           ON oo.FILE_ID = bf.ID

       UNION

       SELECT FILES_ID
       FROM olsc_doc od
         JOIN b_file bf
           ON od.FILES_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM olsc_agreement oa
         JOIN b_file bf
           ON oa.FILE_ID = bf.ID

       UNION

       SELECT LOGOTYPE_DOC_ID
       FROM olsc_org_ext oox
         JOIN b_file bf
           ON oox.LOGOTYPE_DOC_ID = bf.ID

       UNION

       # other

       SELECT FILE_ID
       FROM production_production_file ppf
         JOIN b_file bf
           ON ppf.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM b_ticket_message_2_file btm2f
         JOIN b_file bf
           ON btm2f.FILE_ID = bf.ID

       UNION

       SELECT FILE_ID
       FROM b_posting_file bpf
         JOIN b_file bf
           ON bpf.FILE_ID = bf.ID

       UNION

       SELECT FILES_ID
       FROM alien_doc ad
         JOIN b_file bf
           ON ad.FILES_ID = bf.ID

      # bitrix fucking iblocks

       UNION
        
       # Документы
       SELECT PROPERTY_332
       FROM b_iblock_element_prop_s49 ibep49
       JOIN b_file bf
       ON ibep49.PROPERTY_332 = bf.ID
     )
  AS fds;

# total: 335 901

SELECT count(ID)
FROM b_file;

# total: 360 071 - 335 901 = 24 170 b_file records without references