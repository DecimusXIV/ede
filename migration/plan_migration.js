'use strict';

const Promise = require('bluebird');
const mysql = require('mysql');
const pg = require('pg');
const path = require('path');
const co = require('co');
const crypto = require('crypto');
const cryptopro = require('cryptopro');
const fs = require('fs');

const config = require('../config');
const db = require(path.join(config.application.root, config.application.components, 'db'));

const zakazrfConn = Promise.promisifyAll(
    mysql.createConnection({
        host        : '10.0.103.206',//'10.0.105.26',
        user        : 'estpuser2',//'bitrix',
        password    : 'Hweg73GmgU3}gsu#',//'JruOit34#fsj8',
        database    : 'zakazrf'
    })
);

const referencingTables = {
    'plan_doc'                          : {colName: 'FILE_ID'}
};

const bitrixUploadPath = '/var/www/estp/upload/';
const edeUploadPath = '/tmp';

const batchSize = 10000;

zakazrfConn.connectAsync().then(() => {
    return new Promise((resolve, reject) => {
        zakazrfConn.queryAsync('SELECT count(id) cnt FROM b_file').then(result => {
            //console.log(typeof result + ', ' + JSON.stringify(result));
            resolve(result[0]['cnt']);
        }).catch(err => {
            reject(new Error('could not get total file records: ' + err.toString()));
        });
    });
}).then(total => {
    return co(function* () {
        let offset = 0;
        let notFound = 0;
        let cntByTable = {
            'plan_doc'                          : 0,
        };

        while (offset < total) {
            offset += yield new Promise((resolve, reject) => {
                zakazrfConn.queryAsync('SELECT * FROM b_file ORDER BY id LIMIT ' + offset + ', ' + batchSize).then(results => {
                    return Promise.each(results, bitrixFile => {
                        return new Promise((resolve, reject) => {
                            //console.log(bitrixFile);
                            let edeFile = {};

                            edeFile.id = bitrixFile.ID;
                            edeFile.original_name = bitrixFile.ORIGINAL_NAME;
                            edeFile.size = bitrixFile.FILE_SIZE;
                            edeFile.mime_type = bitrixFile.CONTENT_TYPE;
                            edeFile.creation_timestamp = bitrixFile.TIMESTAMP_X;

                            let refTableName = '';
                            let found = [];
                            Promise.each(Object.keys(referencingTables), tableName => {
                                let query = 'SELECT count(' + referencingTables[tableName].colName + ') cnt FROM '
                                    + tableName + ' WHERE ' + referencingTables[tableName].colName + ' = ' + bitrixFile.ID
                                    + (referencingTables[tableName].cond ? ' AND ' + referencingTables[tableName].cond : '');
                                return zakazrfConn.queryAsync(query).then(result => {
                                    if (result[0]['cnt'] > 0) {
                                        refTableName = tableName;
                                        found.push(tableName);
                                    }
                                }).catch(err => {
                                    console.log('error searching references for ' + bitrixFile.ID
                                        + ' in table ' + tableName + ' with query "' + query +  '": ' + JSON.stringify(err));
                                });
                            }).then(() => {
                                if (!refTableName) {
                                    return Promise.each(Object.keys(referencingLogTables), tableName => {
                                        let query = 'SELECT count(' + referencingLogTables[tableName].colName + ') cnt FROM '
                                            + tableName + ' WHERE ' + referencingLogTables[tableName].colName + ' = ' + bitrixFile.ID
                                            + (referencingLogTables[tableName].cond ? ' AND ' + referencingLogTables[tableName].cond : '');
                                        return zakazrfConn.queryAsync(query).then(result => {
                                            if (result[0]['cnt'] > 0) {
                                                refTableName = tableName;
                                                found.push(tableName);
                                            }
                                        }).catch(err => {
                                            console.log('error searching references for ' + bitrixFile.ID
                                                + ' in table ' + tableName + ' with query "' + query +  '": ' + JSON.stringify(err));
                                        });
                                    });
                                } else {
                                    if (found.length > 2 && found.indexOf('olsc_doc') > -1) {
                                        found.splice(found.indexOf('olsc_doc'), 1);
                                    }
                                    if (found.length === 2) {
                                        if (found.indexOf('tender_announce_doc') > -1
                                            && found.indexOf('tender_protocol_doc') > -1)
                                        {
                                            found.splice(found.indexOf('tender_announce_doc'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('olsc_doc') > -1) {
                                            found.splice(found.indexOf('olsc_doc'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('tender_announce_doc') > -1
                                            && found.indexOf('tender_easuz_doc') > -1)
                                        {
                                            found.splice(found.indexOf('tender_easuz_doc'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('tender_announce_doc') > -1
                                            && found.indexOf('tender_contract_doc') > -1)
                                        {
                                            found.splice(found.indexOf('tender_announce_doc'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('tender_announce_doc') > -1
                                            && found.indexOf('tender_offer_doc') > -1)
                                        {
                                            found.splice(found.indexOf('tender_announce_doc'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('olsc_agreement') > -1
                                            && found.indexOf('olsc_offer_org') > -1)
                                        {
                                            found.splice(found.indexOf('olsc_agreement'), 1);
                                            refTableName = found[0];
                                        }
                                    }
                                    return '';
                                }
                            }).then(() => {
                                if (!refTableName) {
                                    notFound += 1;
                                    throw new Error('no references found');
                                } else {
                                    if (found.length === 2) {
                                        if (found.indexOf('tender_lot_doc_log') > -1
                                            && found.indexOf('tender_protocol_doc_log') > -1)
                                        {
                                            found.splice(found.indexOf('tender_lot_doc_log'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('tender_announce_doc_log') > -1
                                            && found.indexOf('tender_protocol_doc_log') > -1)
                                        {
                                            found.splice(found.indexOf('tender_announce_doc_log'), 1);
                                            refTableName = found[0];
                                        } else if (found.indexOf('tender_contract_doc_log') > -1
                                            && found.indexOf('tender_offer_doc_log') > -1)
                                        {
                                            found.splice(found.indexOf('tender_offer_doc_log'), 1);
                                            refTableName = found[0];
                                        }
                                    }
                                    cntByTable[refTableName] += 1;
                                }

                                if (found.length > 1) {
                                    console.log(bitrixFile.ID + ' referenced in several tables: ' + JSON.stringify(found));
                                }
                                //console.log(bitrixFile.ID + ': ' + refTableName);

                                //if (refTableName.indexOf())
                                let query = 'SELECT count(' + referencingTables[refTableName].colName + ') cnt FROM '
                                    + refTableName + ' WHERE ' + referencingTables[refTableName].colName + ' = ' + bitrixFile.ID
                                    + (referencingTables[refTableName].cond ? ' AND ' + referencingTables[refTableName].cond : '');

                                return zakazrfConn.queryAsync(query);
                                //resolve(results);
                            }).then((queryResult) => {
                                if (queryResult[0]['cnt'] > 1) {
                                    if (!refTableName.endsWith('_log') && ['tender_protocol_doc', 'tender_offer_doc'].indexOf(refTableName) < 1) {
                                        console.log('warning: ' + queryResult[0]['cnt'] + ' records found for '
                                            + bitrixFile.ID + ' in ' + refTableName);
                                    }
                                }

                                let moduleName = '';

                                if (refTableName.startsWith('tender_') || refTableName.startsWith('plan_')) {
                                    edeFile.module_id = 2;
                                    moduleName = 'tender';
                                } else if (refTableName === 'production_production_file') {
                                    edeFile.module_id = 4;
                                    moduleName = 'public';
                                } else {
                                    edeFile.module_id = 1;
                                    moduleName = 'personal';
                                }

                                edeFile.creation_user_id = 0;
                                edeFile.path = path.join(bitrixFile.SUBDIR, bitrixFile.FILE_NAME);

                                if (!fs.existsSync(path.join(bitrixUploadPath, edeFile.path))) {
                                    console.log('WARNING! File ' + edeFile.path + ' does not exist');
                                }

                                return cryptopro.getFileHash(path.join(bitrixUploadPath, edeFile.path));
                            }).then(hash => {
                                edeFile.hash = hash;

                                return db.models.SchemaFile.create(edeFile);
                            }).then(fileRecord => {
                                console.log('file record created: ' + fileRecord.toString());

                                return resolve(results);
                            }).catch(err => {
                                if (err.message !== 'no references found') {
                                    console.log(err.toString() + ', file id: ' + bitrixFile.ID + ', table name: ' + refTableName);
                                }
                                return resolve(results);
                            });
                        });
                    });
                }).then(results => {
                    console.log('processed: ' + (offset + results.length) /*+ ', stats: ' + JSON.stringify(cntByTable)*/);
                    resolve(results.length);
                }).catch(err => {
                    console.log('error ' + err.toString());
                    reject(err);
                })
            });
        }

        return {'processed': offset, 'not found': notFound, 'stats by table' : cntByTable};
    });
}).then(result => {
    console.log('result: ' + JSON.stringify(result));
}).catch(err => {
    console.log('error ' + err.toString());
}).finally(() => {
    zakazrfConn.end();
});