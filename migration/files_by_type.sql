SELECT COUNT(*)
FROM b_file;
# 358702

SELECT count(*)
FROM tender_protocol_doc tpd
  JOIN b_file bf
    ON tpd.FILE_ID = bf.ID;
# 26940

SELECT count(*)
FROM tender_offer_doc tod
  JOIN b_file bf
    ON tod.FILE_ID = bf.ID;
# 202080

SELECT count(*)
FROM tender_offer_correction_doc tocd
  JOIN b_file bf
    ON tocd.FILE_ID = bf.ID;
# 918

SELECT count(*)
FROM tender_lot_doc tld
  JOIN b_file bf
    ON tld.FILE_ID = bf.ID;
# 2611

SELECT count(*)
FROM tender_explanation_doc ted
  JOIN b_file bf
    ON ted.FILE_ID = bf.ID;
# 1277

SELECT count(*)
FROM tender_easuz_doc tesd
  JOIN b_file bf
    ON tesd.FILE_ID = bf.ID;
# 7108

SELECT count(*)
FROM tender_contract_doc tcd
  JOIN b_file bf
    ON tcd.FILE_ID = bf.ID;
# 9008

SELECT count(*)
FROM tender_announce_doc tad
  JOIN b_file bf
    ON tad.FILE_ID = bf.ID;
# 34363

# tender total: 284305

SELECT count(*)
FROM plan_doc pd
  JOIN b_file bf
    ON pd.FILE_ID = bf.ID;
# 1650

# total tender + plan: 285955

SELECT count(*)
FROM olsc_doc od
  JOIN b_file bf
    ON od.FILES_ID = bf.ID;
# 24016

SELECT count(*)
FROM olsc_offer oo
  JOIN b_file bf
    ON oo.FILE_ID = bf.ID;
# 4

SELECT count(*)
FROM olsc_agreement oa
  JOIN b_file bf
    ON oa.FILE_ID = bf.ID;
# 8690

SELECT count(*)
FROM olsc_offer_org ooo
  JOIN b_file bf
    ON ooo.FILE_ID = bf.ID;
# 1419

# total olsc (bitrix): 34129

# total: 320084. где еще 38618, Ави?!

SELECT count(*)
FROM alien_doc od
  JOIN b_file bf
    ON od.FILES_ID = bf.ID;
# 13