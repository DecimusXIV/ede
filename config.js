let config = {};

config.application = {
	root: "/var/www/fileserver/",
    store: "/var/www/fileserver/upload/",
    models: "app/models/",
	controllers: "app/controllers/",
	components: "app/components/",
	validators: 'app/validators/',
	templates: 'app/templates/',
	services: 'app/server/',
	processors: 'app/processors/',
    cert_store: 'cer/',
	temp_upload_path: '',
	tools: 'tools/'
};
config.http = {
	port: 8890,
	host: '127.0.0.1',
	hostname: 'fileserver.local',
	protocol: 'http'
};
config.tcp = {
	port: 8120,
	host: '127.0.0.1'
};
config.debug =
{
	level: 'debug',
	service: true,
	data: true,
	monitor: true
};
config.database = {
	main: {
        type: "postgres",
        login: "devel",
        password: "123456",
        location: "10.0.103.210",
        name: "fileserver",
		debug: false
	},
	zakazrf: {
        type: "mysql",
        login: "estpuser2",
        password: "Hweg73GmgU3}gsu#",
        location: "10.0.103.206",
        name: "zakazrf",
		debug: false
	}
};
config.redis = {
	host: '10.0.103.206',
	port: 6379,
	db: 0
};
config.validation = {
    abortEarly: false,
	stripUnknown: true
};
config.settings = {
	salt: '9c79be15f5e63a4845f11d23fbf78670'
};

module.exports = config;
