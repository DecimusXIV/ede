<?php

require_once "include/tcpdf/tcpdf.php";
require_once "include/fpdi/fpdi.php";

function bcdechex($dec) {
	$hex = '';
	do {
		$last = bcmod($dec, 16);
		$hex = dechex($last).$hex;
		$dec = bcdiv(bcsub($dec, $last), 16);
	} while($dec>0);
	return $hex;
}

if (!isset($argv[1]))
    throw new Exception('Path to pdf file not provided.');

if (!isset($argv[2]))
    throw new Exception('Path to certificate not provided.');

if (!isset($argv[3]))
    throw new Exception('Path to temporary file not provided.');

/** @var TCPDF $pdf */
$pdf = new FPDI();

$pages = $pdf->setSourceFile($argv[1]);
//$pages = $pdf->setSourceFile('/home/mshelekhov/report.pdf');
for ($i = 1; $i <= $pages; $i++)
{
    $pdf->AddPage();
    $page = $pdf->importPage($i);
    $pdf->useTemplate($page, 0, 0);
}

//$cerInfo = openssl_x509_parse('/home/mshelekhov/vasiliev_base64.cer');
$cerInfo = openssl_x509_parse(file_get_contents($argv[2]));

$info = [
    'Name' => $cerInfo['subject']['CN'],
    'Location' => 'ESTP.RU',
    'ContactInfo' => 'http://estp.ru'
];

$pdf->setExternalSignatureInfo(2, $info, 'A');

$pdf->AddPage();

$img = imagecreatefrompng(__DIR__ . '/ecp_on_pdf.png');
$txtColor = imagecolorallocate($img, 65, 65, 180);
$fontFile = __DIR__ . '/timesbd.ttf';
imagettftext($img, 10, 0, 25, 115, $txtColor, $fontFile, 'Сертификат: '.strtoupper(bcdechex($cerInfo['serialNumber'])));
imagettftext($img, 10, 0, 25, 135, $txtColor, $fontFile, 'Владелец: '.$cerInfo['subject']['CN']);
imagettftext($img, 10, 0, 25, 155, $txtColor, $fontFile,
    'Действителен: с '.date('d.m.Y', $cerInfo['validFrom_time_t']).' до '.date('d.m.Y', $cerInfo['validTo_time_t']));
ob_start();
imagepng($img);
imagedestroy($img);
$imgString = ob_get_clean();
$pdf->Image('@'.$imgString, ($pdf->getPageWidth() / 2) - 52, ($pdf->getPageHeight() / 2) - 25, 105, 50, 'PNG');

$pdf->setSignatureAppearance(($pdf->getPageWidth() / 2) - 52, ($pdf->getPageHeight() / 2) - 25, 105, 50);

//$pdf->Output('/home/mshelekhov/imported.signed.pdf', 'F');

$singPosition = $pdf->OutputSignableData($argv[3]);
//$singPosition = $pdf->OutputSignableData('/home/mshelekhov/imported.prep.pdf');

exit((string) $singPosition);
