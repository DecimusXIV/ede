'use strict';

const path = require('path');
const fs = require('fs');
const cryptopro = require('cryptopro');
const Promise = require('bluebird');
const Jimp = require('jimp');
const crypto = require('crypto');
const glob = require('glob');
const Connector = require('soa-protocol').Connector;
const Event = require('soa-protocol').Event;
const readChunk = require('read-chunk');
const imageType = require('image-type');

const config = require(__dirname + '/../../config');
/** @type Db */
const db = require(path.join(config.application.root, config.application.components, 'db'));
/** @type Logger */
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const File = require(path.join(config.application.root, config.application.processors, 'file')).File;

/**
 * Класс, содержащий методы для работы с изображениями
 */
class Image extends File {
    /**
     * Загрука изображения из временной папки
     *
     * @param {UploadableImage}   image           Объект с данными о файле
     * @param {Object}            user            Объект с данными о пользователе
     * @param {Number}            user.id         Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    _upload(image, user) {
        return new Promise((resolve, reject) => {
            let reply = {};
            let files = [image];

            image.dimensions = image.dimensions || [];

            Promise.each(image.dimensions, dimension => {
                let file = {
                    module: image.module,
                    type: image.type
                };
                file.tmpName = image.tmpName + '.' + dimension.width + 'x' + dimension.height + path.extname(image.name);
                file.name = path.parse(image.name).name
                    + '.' + dimension.width + 'x' + dimension.height + path.extname(image.name);

                let tmpPath = path.join(config.application.temp_upload_path, file.tmpName);

                return Jimp.read(path.join(config.application.temp_upload_path, image.tmpName))
                    .then(img => {
                        Promise.promisifyAll(img);
                        return img.resize(dimension.width, dimension.height).writeAsync(tmpPath);
                    })
                    .then(() => {
                        file.size = fs.statSync(tmpPath).size;
                        return cryptopro.getFileHash(tmpPath);
                    })
                    .then(hash => {
                        file.hash = hash;
                        files.push(file);
                    })
                    .catch(err => {
                        // TODO: а надо ли кидать тут Exception, который ведет к reject?
                        throw new Exception('could not resize image to ' + JSON.stringify(dimension), __filename, err, 'system');
                    });
            }).then(dimensions => {
                return Promise.each(files, (file, index) => {
                    let keyName = index === 0
                        ? 'original'
                        : dimensions[index - 1].width + 'x' + dimensions[index - 1].height;
                    return super._upload(file, user).then(fileId => {
                        reply[keyName] = {
                            id: fileId,
                            url: config.http.protocol + config.http.hostname + '/file/'
                                + crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex')
                                + '/' + fileId
                        };
                        if (index > 0) {
                            db.models.SchemaImageResized.create({
                                original_file_id: reply['original']['id'],
                                width: dimensions[index - 1].width,
                                height: dimensions[index - 1].height,
                                file_id: fileId
                            }).catch(err => {
                                logger.warn('resized file info not saved', file, dimensions[index - 1], err);
                            });
                        }
                    }).catch(err => {
                        reply[keyName] = err.toString();
                    })
                });
            }).then(() => {
                resolve(reply);
            }).catch(err => {
                glob(image.tmpName + '*', (error, tmpFiles) => {
                    if (error) {
                        logger.warn('could not delete temp images', image, error);
                    } else {
                        tmpFiles.forEach(tmpFile => {
                            fs.unlink(tmpFile, unlinkErr => {
                                if (unlinkErr)
                                    logger.warn('could not delete temp image', tmpFile, unlinkErr);
                            })
                        })
                    }
                });
                reject(new Exception('image upload failed', __filename, err, err.type || 'system'));
            });
        });
    };

    /**
     * Загрузить изображения синхронно
     *
     * @param {UploadableImage[]}   images      Массив объектов с данными об изображениях
     * @param {Object}              user        Объект сданными о пользователе
     * @param {Number}              user.id     Идентификатор пользователя
     * @returns {Promise<Array>}
     */
    upload(images, user) {
        return new Promise((resolve, reject) => {
            let reply = [];

            Promise.each(images, image => {
                return this._upload(image, user)
                    .then(uploadResults => {
                        reply.push({hash: image.hash, results: uploadResults});
                    })
                    .catch(err => {
                        reply.push({hash: image.hash, error: err.toString()});
                    });
            }).then(() => {
                resolve(reply);
            });
        });
    }

    /**
     * Загрузить изображения асинхронно
     *
     * @param {UploadableImage[]}   images      Массив объектов с данными об изображениях
     * @param {Object}              user        Объект сданными о пользователе
     * @param {Number}              user.id     Идентификатор пользователя
     * @returns {Promise<String>}
     */
    uploadAsync(images, user) {
        return new Promise((resolve, reject) => {
            let connector = new Connector();

            connector.on('connect', () => {
                Promise.each(images, image => {
                    return this._upload(image, user)
                        .then(uploadResults => {
                            connector.write(
                                Event.create({
                                    name: 'ede.imageUploadSuccess',
                                    data: {hash: image.hash, results: uploadResults}
                                }).toString()
                            );
                        })
                        .catch(err => {
                            connector.write(
                                Event.create({
                                    name: 'ede.imageUploadError',
                                    data: {hash: image.hash, error: err.toString()}
                                }).toString()
                            );
                        });
                }).then(() => {
                    connector.disconnect();
                });
            });

            connector.on('error', err => {
                return reject(new Exception('images upload could not be started', __filename, err, 'system'));
            });

            connector.connect();

            resolve('images upload started');
        });
    }

    /**
     * Изменяет размеры исходного изображения, сохраняет как новый файл (если такого еще не существует) и
     * возвращает информацию по этому файлу
     *
     * @param {Array<Object>|Object}    images             Объект с идентификатором файла и размерами или массив таковых
     * @param {Number}                  images.id          Идентификатор файла изображения
     * @param {Array<Object>|Object}    images.dimensions  Объект размеров изображения или массив таковых
     * @returns {Promise<Array>}
     */
    getResized(images) {
        return new Promise((resolve, reject) => {
            let reply = {};

            Promise.each(images, image => {
                reply[image.id] = {};
                return Promise.each(image.dimensions, dimension => {
                    return db.models.SchemaImageResized.find({
                        where: {
                            original_file_id: image.id,
                            width: dimension.width,
                            height: dimension.height
                        }
                    }).then(resizedImage => {
                        if (!resizedImage) {
                            let newImage = {};
                            let tmpPath = '';
                            let user = {};

                            return db.models.SchemaFile.findByPrimary(
                                image.id,
                                {
                                    include: {
                                        model: db.models.SchemaRefModule,
                                        as: 'module'
                                    }
                                }
                            ).then(file => {
                                user.id = file.creation_user_id;
                                newImage.module = file.module.name;
                                newImage.type = file.mime_type;
                                newImage.tmpName = file.original_name + '.' + dimension.width + 'x'
                                    + dimension.height + path.extname(file.path);
                                newImage.name = path.parse(file.path).name
                                    + '.' + dimension.width + 'x' + dimension.height + path.extname(file.path);

                                tmpPath = path.join(config.application.temp_upload_path, newImage.tmpName);

                                return Jimp.read(
                                    path.join(config.application.store, file.path)
                                );
                            }).then(img => {
                                if (!img)
                                    throw new Exception('file is not an image', __filename, image, 'error');

                                Promise.promisifyAll(img);
                                return img.resize(dimension.width, dimension.height).writeAsync(tmpPath);
                            }).then(() => {
                                newImage.size = fs.statSync(tmpPath).size;
                                return cryptopro.getFileHash(tmpPath);
                            }).then(hash => {
                                newImage.hash = hash;
                                return super._upload(newImage, user);
                            }).then(newImageId => {
                                newImage.id = newImageId;
                                return db.models.SchemaImageResized.create({
                                    original_file_id: image.id,
                                    width: dimension.width,
                                    height: dimension.height,
                                    file_id: newImageId
                                });
                            }).then(resizedImage => {
                                return super.getInfo({
                                    id: resizedImage.file_id
                                });
                            }).then(imageFile => {
                                reply[image.id][dimension.width + 'x' + dimension.height] = imageFile[newImage.id];
                            }).catch(err => {
                                throw new Exception('could not resize image ' + image.id + ' to '
                                    + dimension.width + 'x' + dimension.height, __filename, err, 'system');
                            });
                        } else {
                            return super.getInfo({
                                id: resizedImage.file_id
                            }).then(imageFile => {
                                reply[image.id][dimension.width + 'x' + dimension.height] = imageFile[resizedImage.file_id];
                            }).catch(err => {
                                throw new Exception('could not get info for image ' + image.id + ' with dimensions '
                                    + dimension.width + 'x' + dimension.height, __filename, err, 'system');
                            });
                        }
                    }).catch(err => {
                        reply[image.id][dimension.width + 'x' + dimension.height] = err.toString();
                    });
                });
            }).then(() => {
                resolve(reply);
            })
        });
    }
}

module.exports.image = new Image();