'use strict';

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const pdf = Promise.promisifyAll(require('html-pdf'));
const path = require('path');
const crypto = require('crypto');
const _ = require('lodash');
const cryptopro = require('cryptopro');
const mkdirpAsync = Promise.promisify(require('mkdirp'));
const spawn = require('child_process').spawn;
const redis = require('redis');
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

const config = require(__dirname + '/../../config');
/** @type Db */
const db = require(path.join(config.application.root, config.application.components, 'db'));
/** @type Logger */
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const certificates = require(path.join(config.application.root, config.application.components, 'certificates'));
const redisClient = redis.createClient(config.redis);

class Pdf {
    /**
     * Создание документа из формы
     * @param data
     * @param template
     * @param user
     *
     * @returns {Promise}
     */
    create(data, template, user) {
        return new Promise((resolve, reject) => {
            logger.debug('data', 'creationPDF incoming data array', data, __filename);
            logger.debug('data', 'creationPDF incoming data template', template, __filename);
            logger.debug('data', 'creationPDF incoming data user', user, __filename);

            // имя конечного файла
            let name = '';
            // директория конечного файла
            let dir = '';
            // полный путь к конечному файлу
            let fullPath = '';
            // html строка с данными
            let htmlString = '';
            // данные конечного файла
            let metaData = {};

            //Ищем шаблон в базе
            db.models.SchemaTemplate.find({
                where: {
                    name: template.name
                },
                include: [
					{
						model: db.models.SchemaRefModule,
						as: 'module'
					}
				]
            }).then(templateInfo => {
            	if (!templateInfo)
            		throw new Exception('no template found', __filename, template, 'error');

                logger.debug('monitor', 'template info', templateInfo, __filename);

                name = crypto.createHash('md5').update(templateInfo.id + user.id + Date.now() + Math.random).digest('hex');
                logger.debug('monitor', 'file new name', name, __filename);

                //определяем новую директорию для файла
                dir = path.join(config.application.store, templateInfo.module.name, name.substring(0, 3));
                logger.debug('monitor', 'file new directory', dir, __filename);

                metaData.original_name = templateInfo.module.name + ' form.pdf';
                metaData.module_id = templateInfo.module.id;
                metaData.path = path.join(templateInfo.module.name, name.substring(0, 3), name + '.pdf');

                //определяем путь до шаблона
                let fileName = path.join(
                    config.application.root,
                    config.application.templates,
                    templateInfo.module.name,
                    templateInfo.name + '.html'
                );

                logger.debug('monitor', 'template path detected', fileName, __filename);

                //читаем содержимое шаблона
                return fs.readFileAsync(fileName).catch(err => {
                    throw new Exception('read file fault', __filename, err, 'system');
                });
            }).then(schema => {
                //скрещиваем с шаблонизатором
                htmlString = _.template(schema)(data);

                logger.debug('monitor', 'new file content', htmlString, __filename);

                //проверяем, существует ли такая директория
                return fs.statAsync(dir).catch(err => {
                    //если нет - создаём
                    if (err.code === 'ENOENT') {
                        logger.debug('monitor', 'file directory is absent', dir, __filename);
                        return mkdirpAsync(dir).catch(err => {
                            throw new Exception('error creating directory for file', __filename, err, 'system');
                        });
                    } else {
                        throw new Exception('error accessing directory for file', __filename, err, 'system');
                    }
                });
            }).then(() => {
                fullPath = path.join(dir, name + '.pdf');

                //создаем pdf
                return Promise.promisifyAll(pdf.create(htmlString)).toFileAsync(fullPath).catch(err => {
                    throw new Exception('could not save pdf file', __filename, err, 'system');
                });
            }).then(result => {
                logger.debug('monitor', 'pdf creator result', result, __filename);

                //собираем информацию о новом файле
                return fs.statAsync(result.filename).catch(err => {
                    throw new Exception('new file fault', __filename, err, 'system');
                });
            }).then(stats => {
                if (stats.size === 0)
                    throw new Exception('new file size zero', __filename, stats, 'system');

                metaData.size = stats.size;

                logger.debug('monitor', 'new file info', stats, __filename);

                //определяем хэш файла
                return cryptopro.getFileHash(fullPath).catch(err => {
                    throw new Exception('hash fault', __filename, err, 'system');
                });
            }).then(hash => {
                logger.debug('monitor', 'new file hash', hash, __filename);

                metaData.mime_type = 'application/pdf';
                metaData.hash = hash;
                metaData.creation_user_id = user.id;

                logger.debug('monitor', 'new file meta', metaData, __filename);

                //записываем информацию о файле
                return db.models.SchemaFile.create(metaData);
            }).then(file => {
                logger.debug('monitor', 'file upload result', file, __filename);

                let hash = crypto.createHash('md5').update(file.id.toString() + config.settings.salt).digest('hex');

                resolve({
                    id: file.id,
                    url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + file.id
                });
            }).catch(err => {
                reject(new Exception('could not create pdf from data', __filename, err, err.type || 'system'));
            });
        });
    };

    /**
     * Подготовка временного файла для подписания
     *
     * @param {Object}  file        Объект с информацией о файле
     * @param {Number}  file.id     Идентификатор файла
     * @param {Object}  user        Объект с информацией о пользователе
     * @param {Number}  user.id     Идентификатор пользователя
     * @returns Promise<Object>
     */
    prepareSignableFile(file, user) {
        return new Promise((resolve, reject) => {
            Promise.all([
                db.models.SchemaFile.find({
                    where: {
                        id: file.id,
                        cancellation_timestamp: null
                    }
                }),
                certificates.getCertificatePath(user.id)
            ]).spread((fileInfo, cerPath) => {
                return new Promise((resolve, reject) => {
                    if (!fileInfo)
                        reject(new Exception('no file found with such id', __filename, file.id, 'error'));

                    if (fileInfo.mime_type.indexOf('pdf') < 0)
                        reject(new Exception('file is not in pdf format', __filename, file.id, 'error'));

                    const tmpFilePath = path.join(
                        'tmp/',
                        crypto
                            .createHash('md5')
                            .update(file.id.toString() + user.id.toString() + Date.now().toString())
                            .digest('hex') + '.pdf'
                    );
                    const pdfSigner = spawn('php', [
                        path.join(config.application.root, config.application.tools, 'pdf-signer/getSignableData.php'),
                        path.join(config.application.store, fileInfo.path),
                        cerPath,
                        path.join(config.application.store, tmpFilePath)
                    ]);
                    let signPosition = 0;
                    let errData = '';

                    pdfSigner.stderr.on('data', data => {
                        errData = data.toString();
                    });

                    pdfSigner.stdout.on('data', data => {
                        signPosition = parseInt(data.toString());
                    });

                    pdfSigner.on('exit', code => {
                        if (code !== 0)
                            return reject(new Exception('error executing php script', __filename, errData, 'system'));

                        redisClient.hmset('ede:pdf:' + file.id,
							'path', tmpFilePath,
							'signPosition', signPosition,
							'EX', 1200
						);

                        const hash = crypto
                            .createHash('md5')
                            .update(file.id.toString() + 'pdftmp' + config.settings.salt).digest('hex');

                        return resolve(config.http.protocol + config.http.hostname + '/tmp/' + hash + '/' + file.id);
                    });
                });
            }).then(url => {
                resolve({url: url});
            }).catch(err => {
                reject(new Exception('could not prepare pdf for signing', __filename, err, err.type || 'system'));
            })
        });
    }

    /**
     * Сохранение подписи в файл
     *
     * @param {Object}  file                Объект с информацией о файле
     * @param {Number}  file.id             Идентификатор файла
     * @param {String}  file.signature      Подпись файла в формате base64
     * @param {Object}  user                Объект с информацией о пользователе
     * @param {Number}  user.id             Идентификатор пользователя
     * @returns Promise<Object>
     */
    saveSignature(file, user) {
        return new Promise((resolve, reject) => {
            Promise.all([
                db.models.SchemaFile.find({
                    where: {
                        id: file.id,
                        cancellation_timestamp: null
                    }
                }),
                certificates.getCertificatePath(user.id),
                redisClient.hgetallAsync('ede:pdf:' + file.id)
            ]).spread((fileInfo, cerPath, tmpFileInfo) => {
                return new Promise((resolve, reject) => {
                    if (!fileInfo)
                        reject(new Exception('no file found with such id', __filename, file.id, 'error'));

                    if (!tmpFileInfo)
                        reject(new Exception('no temporary pdf file for signing', __filename, file.id, 'error'));

                    const tmpFileFullPath = path.join(config.application.store, tmpFileInfo.path);

                    cryptopro.verifyFileSign(tmpFileFullPath, file.signature, cerPath).catch(err => {
                        return reject(new Exception('signature not verified', __filename, err, 'error'));
                    }).then(() => {
                        return fs.readFileAsync(tmpFileFullPath);
                    }).then(pdf => {
                        const signature = Buffer.from(file.signature, 'base64').toString('hex');
                        const signedPdf = Buffer.concat([
                            pdf.slice(0, tmpFileInfo.signPosition),
                            Buffer.from('<'),
                            Buffer.from(signature),
                            Buffer.from('0'.repeat(11742 - Buffer.byteLength(signature))),
                            Buffer.from('>'),
                            pdf.slice(tmpFileInfo.signPosition)
                        ]);

                        return fs.writeFileAsync(path.join(config.application.store, fileInfo.path), signedPdf);
                    }).then(() => {
                        return resolve();
                    }).catch(err => {
                        return reject(new Exception('could not insert signature into pdf', __filename, err, 'system'));
                    }).finally(() => {
						fs.unlink(tmpFileFullPath, unlinkErr => {
							if (unlinkErr)
								logger.warn('could not delete temporary pdf file', tmpFileInfo.path, __filename);

							redisClient.del('ede:pdf:' + file.id);
						});
                    });
                });
            }).then(() => {
                const hash = crypto.createHash('md5').update(file.id.toString() + config.settings.salt).digest('hex');

                resolve({
                    id: file.id,
                    url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + file.id
                });
            }).catch(err => {
                reject(new Exception('could not save pdf signature', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Получение полного пути к временному файлу
     *
     * @param {Object}  data        Объект с данными о файле
     * @param {Number}  data.id     Идентификатор файла
     * @returns {Promise<Object>}
     */
    getTmpPath(data) {
        return new Promise((resolve, reject) => {
            logger.debug('monitor', 'getTmpPath function incoming data', data);

            let reply = {};

            db.models.SchemaFile.findByPrimary(data.id).then(file => {
                if (!file)
                    throw new Exception('file with given id not found', __filename, data.id, 'error');

                logger.debug('monitor', 'getTmpPath function file data', file);

                reply.name = file.original_name;
                return redisClient.hgetAsync('ede:pdf:' + data.id, 'path');
            }).then((path) => {
                if (!path)
                    throw new Exception('no temporary file exists', __filename, data.id, 'error');

                reply.path = path;
                resolve(reply);
            }).catch(err => {
                reject(new Exception('could not get real temp file path', __filename, err, err.type || 'system'));
            });
        });
    };
}

module.exports.pdf = new Pdf();