'use strict';

const path = require('path');
const crypto = require('crypto');
const moment = require('moment');
const cryptopro = require('cryptopro');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const x509 = require('x509');
const mkdirp = require('mkdirp');
const JSZip = require('jszip');

const config = require(__dirname + '/../../config');
/** @type Db */
const db = require(path.join(config.application.root, config.application.components, 'db'));
/** @type Logger */
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const certificates = require(path.join(config.application.root, config.application.components, 'certificates'));
const Connector = require('soa-protocol').Connector;
const Event = require('soa-protocol').Event;

/**
 * Класс, содержащий методы для работы с файлами
 */
class File {
    /**
     * Получение информации о файле
     *
     * @param {Object}              file       Объект с данными о файле
     * @param {number|number[]}     file.id    Идентификатор(ы) файла
     * @returns {Promise<Array>}
     */
    getInfo(file) {
        return new Promise((resolve, reject) => {
            logger.debug('monitor', 'getFileInfo function incoming data', file);

            db.models.SchemaFile.findAll({
                where: {
                    id: file.id,
                    cancellation_timestamp: null,
                },
                include: [
                    {
                        model: db.models.SchemaRefModule,
                        as: 'module'
                    },
                    {
                        model: db.models.SchemaSignature,
                        as: 'signatures'
                    }
                ]
            }).then(files => {
                logger.debug('monitor', 'getInfo function file data', files);

                let reply = {};

                if (files.length < 1)
                    return resolve(reply);

                files.forEach(file => {
                    let item = {};

                    let hash = crypto.createHash('md5').update(file.id.toString() + config.settings.salt).digest('hex');

                    item.url = config.http.protocol + config.http.hostname + '/file/' + hash + '/' + file.id;
                    item.name = file.original_name;
                    item.size = file.size;
                    item.type = file.mime_type;
                    item.createdBy = file.creation_user_id;
                    item.createdAt = file.creation_timestamp;
                    item.controlHash = hash;
                    item.sign = {
                        hash: file.hash,
                        base64: Buffer.from(file.hash, 'hex').toString('base64')
                    };
                    item.module = {
                        name: file.module.name,
                        title: file.module.title
                    };
                    item.signatures = file.signatures.map(value => {
                       delete value.fileId;
                    });

                    reply[file.id] = item;
                });

                resolve(reply);
            }).catch(err => {
                reject(new Exception('could not retrieve file info', __filename, err, 'system'));
            });
        });
    };

    /**
     * Получение полного пути к файлу
     *
     * @param {Object}  data        Объект с данными о файле
     * @param {Number}  data.id     Идентификатор файла
     * @returns {Promise<Object>}
     */
    getPath(data) {
        return new Promise((resolve, reject) => {
            logger.debug('monitor', 'getPath function incoming data', data);

            db.models.SchemaFile.findByPrimary(data.id).then(file => {
                /** @type {SchemaFile} file */
                if (!file)
                    throw new Exception('file with given id not found', __filename, data, 'error');

                logger.debug('monitor', 'getPath function file data', file);
                resolve({name: file.original_name, path: file.path});
            }).catch(err => {
                reject(new Exception('could not get real file path', __filename, err, err.type || 'system'));
            });
        });
    };

    /**
     * Упаковка файла и его подписей в один архив
     *
     * @param {Object}  data                Объект с данными запроса
     * @param {Number}  data.id             Идентификатор файла
     * @param {Number}  [data.announceID]   Идентификатор извещения, к которому прикреплен файл
     * @param {Number}  [data.offerID]      Идентификатор заявки, к которой прикреплен файл
     * @param {Number}  [data.contractID]   Идентификатор договора, к которому прикреплен файл
     * @param {Number}  [data.lotID]        Идентификатор лота, к которому прикреплен файл
     * @param {String}  [data.type]         Тип архива документов
     * @returns {Promise<Object>}
     */
    createArchive(data) {
        return new Promise((resolve, reject) => {
            const prefix = {
                'AD': 'Add',
                'CL': 'Close',
                'EN': 'Ensuring',
                'P1': 'FirstPart',
                'P2': 'SecondPart',
                'BG': 'BankGuarantee',
                'CT': 'Contract',
                'TD': 'Tender',
                'EX': 'Explanation',
                'PC': 'Protocol',
                'PT': 'Platform',
                'default': ''
            }[data['type']];

            let zipName = '';

            db.models.SchemaFile.find({
                where: {
                    id: data.id,
                    cancellation_timestamp: null,
                },
                include: [
                    {
                        model: db.models.SchemaSignature,
                        as: 'signatures'
                    }
                ]
            }).then(fileInfo => {
                if (data['contractID']) {
                    /**
                     * Неявная защита от скачивания подписей к файлу, отличному от файла
                     * контракта, так как у файла контракта [в стадии договор подписан]
                     * минимум две подписи.
                     */
                    if (fileInfo.signatures.length < 2)
                        throw new Exception('Недостаточно подписей файла найдено.', __filename, data.id, 'error');

                    zipName = `${prefix}Doc${data.id}_Contract${data['contractID']}.zip`;
                } else if (data['offerID']) {
                    zipName = `${prefix}Doc${data.id}_Offer${data['offerID']}.zip`;
                } else if (data['lotID']) {
                    zipName = `${prefix}Doc${data.id}_Lot${data['lotID']}.zip`;
                } else if (data['announceID']) {
                    zipName = `${prefix}Doc${data.id}_Announce${data['announceID']}.zip`;
                } else {
                    zipName = `${prefix}Doc${data.id}.zip`;
                }

                const dir = path.join(config.application.store, 'tmp');
                if (!fs.existsSync(dir))
                    fs.mkdirSync(dir);

                const zipPath = path.join(dir, zipName);

                const zip = new JSZip();
                zip.file(fileInfo.name, fs.readFileSync(path.join(config.application.store, fileInfo.path)));

                fileInfo.signatures.forEach((item, index) => {
                    let passport = ''
                        + `Дата подписания: ${item['signTimestamp']}\r\n`
                        + `ФИО владельца сертификата: ${item['name']}\r\n`
                        + `Должность владельца: ${item['position']}\r\n`
                        + `Наименование организации: ${item['orgName']}\r\n`;

                    if (fileInfo.signatures.length > 1) {
                        zip.file(`Signature(${index}).zip`, item.signature, {base64: true});
                        zip.file(`Passport(${index}).txt`, passport);
                    } else {
                        zip.file(`Signature.zip`, item.signature, {base64: true});
                        zip.file(`Passport.txt`, passport);
                    }
                });

                zip
                    .generateNodeStream({
                        compression: 'DEFLATE',
                        compressionOptions: {
                            level: 6
                        }
                    })
                    .pipe(fs.createWriteStream(zipPath))
                    .on('error', err => {
                        throw new Exception('could not save zip file', __filename, err, 'system');
                    })
                    .on('finish', () => {
                        return resolve({name: zipName, path: zipPath});
                    });
            }).catch(err => {
                reject(new Exception('could not create archive', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Загрузка файла из временной папки
     *
     * @param {UploadableFile}  file            Объект с данными о загружаемом файле
     * @param {Object}          user            Объект с данными о пользователе
     * @param {Number}          user.id         Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    _upload(file, user) {
        return new Promise((resolve, reject) => {
            logger.debug('monitor', 'uploaded file data', file);

            //получаем расширение файла
            let extension = path.extname(file.name);
            logger.debug('monitor', 'uploaded file extension', extension);

            //создаем имя файла
            let name = crypto.createHash('md5').update(file.name + user.id + Date.now() + Math.random()).digest('hex');
            logger.debug('monitor', 'uploaded file new name', name);

            // путь к временному файлу
            let tmpPath = path.join(config.application.temp_upload_path, file.tmpName);
            // имя хранимого файла
            let permName = name + '.' + extension;
            // директория хранимого файла
            let permDir = path.join(file.module, permName.substring(0, 3));
            // путь к директории хранимого файла
            let permDirFull = path.join(config.application.store, permDir);
            // полный путь к хранимому файлу
            let permPathFull = path.join(permDirFull, permName);

            // извлекаем информацию о модуле, к которому относится файл
            db.models.SchemaRefModule.find({
                where: {
                    name: file.module
                }
            }).then(module_result => {
                return new Promise((resolve, reject) => {
                    logger.debug('monitor', 'module result', module_result);

                    if (!module_result)
                        reject(new Exception('no such module', __filename, module_result, 'error'));

                    //данные файла
                    let metaData = {};

                    metaData.original_name = file.name;
                    metaData.size = file.size;
                    metaData.mime_type = file.type;
                    metaData.module_id = module_result.id;
                    metaData.path = path.join(permDir, permName);
                    metaData.hash = file.hash;
                    metaData.creation_user_id = user.id;

                    logger.debug('monitor', 'uploaded file meta data', metaData);

                    logger.debug('monitor', 'creating file directory', permDirFull);

                    //создаем директорию
                    mkdirp(permDirFull, err => {
                        //если существует - ну и отлично
                        if (err) {
                            if (err.code === 'EEXIST') {
                                logger.debug('monitor', 'directory for file already exists', permDirFull);
                            } else {
                                logger.error('monitor', 'error creating new directory', err);
                                return reject(new Exception('could not create a directory for file', __filename, err, 'system'));
                            }
                        }

                        //определяем поток для чтения
                        let source = fs.createReadStream(tmpPath);

                        logger.debug('monitor', 'file directory source', source);

                        //определяем поток для записи
                        let destination = fs.createWriteStream(permPathFull);

                        logger.debug('monitor', 'file directory destination', destination);

                        //если скачиваение закончилось
                        destination.on('finish', () => {
                            logger.debug('monitor', 'file upload done', null);

                            //смотрим на новый файл
                            fs.stat(permPathFull, (err, stats) => {
                                if (err) {
                                    return reject(new Exception('file not created', __filename, err, 'system'));
                                } else if (stats.size === 0) {
                                    return reject(new Exception('file is empty', __filename, stats, 'system'));
                                } else if (stats.size !== file.size) {
                                    return reject(new Exception('file copied incompletely', __filename, stats, 'system'));
                                } else {
                                    return resolve(db.models.SchemaFile.create(metaData));
                                }
                            });
                        });

                        destination.on('error', err => {
                            logger.warn('monitor', 'file not uploaded', err);
                            return reject(
                                new Exception('error while trying to copy temp file to destination', __filename, err, 'system')
                            );
                        });

                        source.on('error', err => {
                            logger.warn('monitor', 'file not uploaded', err);
                            destination.end();
                        });

                        //качаем файл
                        source.pipe(destination);
                    });
                });
            }).then(saveResult => {
                /** @type {SchemaFile} file_result */
                logger.debug('monitor', 'file upload result', saveResult);

                /** TODO временно сохраняем еще и в b_file */
                /*return db.zakazrf.models.SchemaBFile.create({
                    ID: file_result.id,
                    MODULE_ID: 'ede',
                    FILE_SIZE: file_result.size,
                    CONTENT_TYPE: file_result.type,
                    SUBDIR: path.dirname(file_result.path),
                    FILE_NAME: path.basename(file_result.path),
                    ORIGINAL_NAME: file_result.name
                });*/
                resolve(saveResult.id);
            /*}).then(bfile_result => {
                resolve(bfile_result.ID);*/
            }).catch(err => {
                //удаляем сохраненный файл
                fs.unlink(permPathFull, err => {
                    if (err)
                        logger.warn('could not remove permanent file on error', permPathFull, __filename);
                });

                reject(new Exception('file upload failed', __filename, err, err.type || 'system'));
            }).finally(() => {
                //удаляем временный файл
                fs.unlink(tmpPath, err => {
                    if (err)
                        logger.warn('could not remove temporary file', tmpPath, err, __filename);
                });
            });
        });
    };

    /**
     * Копирует файл и создает для него запись в БД
     *
     * @param {Object}  file		Объект с данными о файле
     * @param {Number}  file.id		Идентификатор файла
     * @param {Object}	user		Объект с данными о пользователе
     * @param {Number}	user.id		Идентификатор пользователя
     * @returns {Promise<Number>}
     */
    _copy(file, user) {
        return new Promise((resolve, reject) => {
            db.models.SchemaFile.findByPrimary(file.id, {
                include: [
                    {
                        model: db.models.SchemaRefModule,
                        as: 'module'
                    }
                ]
            }).then(fileInfo => {
                if (!fileInfo)
                    throw new Exception('no file with such id', __filename, file.id, 'error');

                //получаем расширение файла
                let extension = path.extname(fileInfo.path);

                //создаем имя файла
                let newName = crypto
                    .createHash('md5')
                    .update(fileInfo.path + user.id + Date.now() + Math.random())
                    .digest('hex');

                // имя нового файла
                let newNameFull = newName + '.' + extension;
                // директория нового файла
                let newDir = path.join(fileInfo.module.name, newNameFull.substring(0, 3));
                // путь к директории нового файла
                let newDirFull = path.join(config.application.store, newDir);
                // полный путь к новому файлу
                let newPathFull = path.join(newDirFull, newNameFull);

                return new Promise((resolve, reject) => {
                    //данные файла
                    let metaData = {};

                    metaData.original_name = fileInfo.original_name;
                    metaData.size = fileInfo.size;
                    metaData.mime_type = fileInfo.mime_type;
                    metaData.module_id = fileInfo.module_id;
                    metaData.path = path.join(newDir, newNameFull);
                    metaData.hash = fileInfo.hash;
                    metaData.creation_user_id = user.id;

                    //создаем директорию
                    mkdirp(newDirFull, err => {
                        //если существует - ну и отлично
                        if (err) {
                            if (err.code === 'EEXIST') {
                                logger.debug('monitor', 'directory for file already exists', newDirFull);
                            } else {
                                logger.error('monitor', 'error creating new directory', err);
                                return reject(new Exception('could not create a directory for file', __filename, err, 'system'));
                            }
                        }

                        //определяем поток для чтения (оригинальный файл)
                        let source = fs.createReadStream(path.join(config.application.store, fileInfo.path));

                        //определяем поток для записи
                        let destination = fs.createWriteStream(newPathFull);

                        //если копирование закончилось
                        destination.on('finish', () => {
                            //смотрим на новый файл
                            fs.stat(newPathFull, (err, stats) => {
                                if (err) {
                                    return reject(new Exception('file not created', __filename, err, 'system'));
                                } else if (stats.size === 0) {
                                    return reject(new Exception('file is empty', __filename, newPathFull, 'system'));
                                } else if (stats.size !== file.size) {
                                    return reject(new Exception('file copied incompletely', __filename, newPathFull, 'system'));
                                } else {
                                    return resolve(db.models.SchemaFile.create(metaData));
                                }
                            });
                        });

                        destination.on('error', err => {
                            return reject(
                                new Exception('error while trying to copy file', __filename, err, 'system')
                            );
                        });

                        source.on('error', err => {
                            destination.end();
                        });

                        //качаем файл
                        source.pipe(destination);
                    });
                });
            }).then(saveResult => {
                /** @type {SchemaFile} file_result */
                logger.debug('monitor', 'file copy result', saveResult);

                /** TODO временно сохраняем еще и в b_file */
                /*return db.zakazrf.models.SchemaBFile.create({
                    ID: saveResult.id,
                    MODULE_ID: 'ede',
                    FILE_SIZE: saveResult.size,
                    CONTENT_TYPE: saveResult.type,
                    SUBDIR: path.dirname(saveResult.path),
                    FILE_NAME: path.basename(saveResult.path),
                    ORIGINAL_NAME: saveResult.name
                });*/
                resolve(saveResult.id);
                /*}).then(bfile_result => {
                    resolve(bfile_result.ID);*/
            }).catch(err => {
                if (['file is empty', 'file copied incompletely'].includes(err.message))
                    //удаляем сохраненный файл
                    fs.unlink(err.context, unlinkErr => {
                        if (unlinkErr)
                            logger.warn('could not remove copied file on error', err.context, __filename);
                    });

                reject(new Exception('file upload failed', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Загрузить файлы синхронно
     *
     * @param {UploadableFile[]}    files       Массив объектов с данными о файлах
     * @param {Object}              user        Объект сданными о пользователе
     * @param {Number}              user.id     Идентификатор пользователя
     * @returns {Promise<Array>}
     */
    upload(files, user) {
        return new Promise((resolve, reject) => {
            let reply = {};

            Promise.each(files, file => {
                return this._upload(file, user)
                    .then(fileId => {
                        let hash = crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex');

                        reply[file.hash] = {
                            id: fileId,
                            url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId
                        };
                    })
                    .catch(err => {
                        logger.error('file upload failed', err);

                        reply[file.hash] = err.toString();
                    });
            }).then(() => {
                resolve(reply);
            });
        });
    }

    /**
     * Загрузить файлы асинхронно
     *
     * @param {UploadableFile[]}    files       Массив объектов с данными о файлах
     * @param {Object}              user        Объект сданными о пользователе
     * @param {Number}              user.id     Идентификатор пользователя
     * @returns {Promise<String>}
     */
    uploadAsync(files, user) {
        return new Promise((resolve, reject) => {
            let connector = new Connector();

            connector.on('connect', () => {
                Promise.each(files, file => {
                    return this._upload(file, user)
                        .then(fileId => {
                            let hash = crypto.createHash('md5')
                                .update(fileId.toString() + config.settings.salt)
                                .digest('hex');
                            connector.write(
                                Event.create({
                                    name: 'ede.fileUploadSuccess',
                                    data: {
                                    	hash: file.hash,
										id: fileId,
										url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId
                                    }
                                }).toString()
                            );
                        })
                        .catch(err => {
                            connector.write(
                                Event.create({
                                    name: 'ede.fileUploadError',
                                    data: {hash: file.hash, error: err.toString()}
                                }).toString()
                            );
                        });
                }).then(() => {
                    connector.disconnect();
                });
            });

            connector.on('error', err => {
                return reject(new Exception('files upload could not be started', __filename, err, 'system'));
            });

            connector.connect();

            resolve('files upload started');
        });
    }

    /**
     * Пометить на удаление
     *
     * @param {Object}          file            Объект с данными о файле
     * @param {Array<Number>}   file.id         Идентификатор(ы) файла(ов)
     * @param {Object}          user            Объект с данными о пользователе
     * @param {Number}          user.id         Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    markDeleted(file, user) {
        return new Promise((resolve, reject) => {
            logger.debug('monitor', 'markAsDelete function incoming data', {file: file, user: user}, __filename);

            let results = {};
            db.models.SchemaFile.findAll({
                where: {
                    id: file.id,
                    cancellation_timestamp: null,
                },
            }).then(file_results => {
                logger.debug('monitor', 'markDeleted function file', file_results, __filename);

                if (file_results.length < 1)
                    throw new Exception('file records not found', __filename, file, 'error');

                let currentId = null;
                return Promise.each(file_results, file_result => {
                    currentId = file_result.id;

                    //выставляем флаг удаления
                    file_result.cancellation_timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
                    file_result.cancellation_user_id = user.id;

                    return file_result.save();
                }).then(save_result => {
                    results[currentId] = true;
                }).catch(err => {
                    results[currentId] = err.toString();
                });
            }).then(() => {
                resolve(results);
            }).catch(err => {
                reject(new Exception('could not mark file deleted', __filename, err, err.type || 'system'));
            });
        });
    };

    /**
     * Сохранить подпись хэша файла
     *
     * @param {Object}  file            Объект с данными о файле
     * @param {Number}  file.id         Идентификатор файла
     * @param {String}  file.signature  Подпись хэша файла
     * @param {Object}  user            Объект с данными о пользователе
     * @param {Number}  user.id         Идентификатор пользователя
     * @param {Number}  user.orgId      Идентификатор организации пользователя
     * @returns {Promise<Number>}
     */
    sign(file, user) {
        return new Promise((resolve, reject) => {
            let cerInfo = '';
            Promise.all([
                db.models.SchemaFile.find({
                    where: {
                        id: file.id,
                        cancellation_timestamp: null
                    }
                }),
                certificates.getCertificatePath(user.id)
            ]).spread((fileResult, cerPath) => {
                if (!fileResult)
                    throw new Exception('file record not found', __filename, file, 'error');

				cerInfo = x509.parseCert(cerPath);

				return cryptopro.verifyHashSign(
                    path.join(config.application.store, fileResult.path),
                    fileResult.hash,
                    file.signature,
                    cerPath
                );
            }).then(() => {
                return db.models.SchemaSignature.create({
                    fileId: file.id,
                    orgId: user.orgId,
                    userId: user.id,
                    signature: file.signature,
                    orgName: cerInfo.subject.organizationName,
                    position: cerInfo.subject.title,
                    name: cerInfo.subject.commonName,
                    validTill: cerInfo.notAfter
                });
            }).then(signature => {
                resolve({'id': signature.fileId});
            }).catch((err) => {
                reject(new Exception('could not save file signature', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Проверить корректность подписи хэша файла пользователем
     *
     * @param {Object}  file               Объект с данными о файле
     * @param {Number}  file.id            Идентификатор файла
     * @param {String}  file.signature     Проверяемая подпись
     * @param {Object}  user               Оъект с данными пользователя
     * @param {Number}  user.id            Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    verifySignature(file, user) {
        return new Promise((resolve, reject) => {
            Promise.all([
                db.models.SchemaFile.find({
                    where: {
                        id: file.id,
                        cancellation_timestamp: null
                    }
                }),
                certificates.getCertificatePath(user.id)
            ]).spread((fileResult, cerPath) => {
                if (!fileResult)
                    throw new Exception('file record not found', __filename, file, 'error');

                return cryptopro.verifyHashSign(
                    path.join(config.application.store, fileResult.path),
                    fileResult.hash,
                    file.signature,
                    cerPath
                );
            }).then(() => {
                resolve({[file.id]: true});
            }).catch(err => {
                reject(new Exception('could not verify file hash signature', __filename, err, err.type || 'system'));
            });
        });
    }
}

module.exports.File = File;
module.exports.file = new File();
