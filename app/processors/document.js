'use strict';

const path = require('path');
const crypto = require('crypto');
const Promise = require('bluebird');

const config = require(__dirname + '/../../config');
/** @type Db */
const db = require(path.join(config.application.root, config.application.components, 'db'));
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const certificates = require(path.join(config.application.root, config.application.components, 'certificates'));
const File = require('file').File;

/**
 * Класс, содержащий методы для работы с документами
 * @extends File
 */
class Document extends File {
    /**
     * Загружает новый документ
     *
     * @param {Object}              document              Объект с данными о документе
     * @param {UploadableFile}      document.file         Объект с данными о загружаемом файле документа
     * @param {string}              document.type         Тип документа
     * @param {string}              document.language     Язык документа
     * @param {Object}              user                  Объект с данными о пользователе
     * @param {number}              user.id               Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    upload(document, user) {
        return new Promise((resolve, reject) => {
            super._upload(document.file, user).then(fileId => {
                return new Promise((resolve, reject) => {
                    const hash = crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex');
                    let result = {
                        fileId: fileId,
                        url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId,
                        type: document.type
                    };

                    db.models.SchemaRefDocumentType.find({
						where: {
							name: document.type
						}
					}).then(type => {
                        if (!type)
                            throw new Exception('document type not found', __filename, document.type, 'error');

                        return db.connection.transaction(t => {
                            return db.models.SchemaDocument.create(
                                {type_id: type.id},
                                {transaction: t}
                            ).then(doc => {
                                result.id = doc.id;

                                return db.models.SchemaDocumentVersion.create(
                                    {
                                        doc_id: doc.id,
                                        version_number: 1,
                                        subversion_number: 0,
                                        creation_user_id: user.id
                                    },
                                    {transaction: t}
                                );
                            }).then(docVersion => {
                                result.versionNumber = docVersion.version_number;
                                result.subversionNumber = docVersion.subversion_number;
                                result.language = document.language;

                                return db.models.SchemaDocumentInstance.create(
                                    {
                                        doc_version_id: docVersion.id,
                                        lang_code: document.language,
                                        file_id: fileId,
                                        creation_user_id: user.id
                                    },
                                    {transaction: t}
                                );
                            });
                        });
                    }).then(() => {
                        resolve(result);
                    }).catch(reject)
                });
            }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(new Exception('failed to upload document', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Добавляет новую мажорную версию документа
     *
     * @param {Object}              document                Объект с данными о документе
     * @param {string}              document.id             Идентификатор документа
     * @param {UploadableFile}      document.file           Объект с данными о загружаемом файле документа
     * @param {string}              document.language       Язык документа
     * @param {Object}              user                    Объект с данными о пользователе
     * @param {number}              user.id                 Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    addVersion(document, user) {
        return new Promise((resolve, reject) => {
            super._upload(document.file, user).then(fileId => {
                return new Promise((resolve, reject) => {
                    const hash = crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex');
                    let result = {
                        fileId: fileId,
                        url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId
                    };

                    db.models.SchemaDocument.findByPrimary(document.id).then(doc => {
                        if (!doc)
                            throw new Exception('document record not found', __filename, document, 'error');

                        result.id = doc.id;

						return db.models.SchemaDocumentVersion.max('version_number', {
							where: {
								doc_id: doc.id
							}
						});
                    }).then(currentVersion => {
                        return db.connection.transaction(t => {
                            return db.models.SchemaDocumentVersion.create(
                                {
                                    doc_id: document.id,
                                    version_number: currentVersion + 1,
                                    subversion_number: 0,
                                    creation_user_id: user.id
                                },
                                {transaction: t}
                            ).then(docVersion => {
                                result.versionNumber = docVersion.version_number;
                                result.subversionNumber = docVersion.subversion_number;
                                result.language = document.language;

                                return db.models.SchemaDocumentInstance.create(
                                    {
                                        doc_version_id: docVersion.id,
                                        lang_code: document.language,
                                        file_id: fileId,
                                        creation_user_id: user.id
                                    },
                                    {transaction: t}
                                );
                            });
                        });
                    }).then(() => {
                        resolve(result);
                    }).catch(reject);
                });
            }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(new Exception('could not add document version', __filename, err, err.type || 'system'));
            });
        });
    }

	/**
	 * Создает новую мажорную версию документа путем копирования существующей версии
	 *
	 * @param {Object}              document              		Объект с данными о копируемом документе
	 * @param {String}              document.id           		Идентификатор документа
	 * @param {Number}              document.versionNumber      Номер мажорной версии документа
	 * @param {Number}              document.subversionNumber   Номер минорной версии документа
	 * @param {String}              document.language   		Язык документа
	 * @param {Object}              user                  		Объект с данными о пользователе
	 * @param {Number}              user.id               		Идентификатор пользователя
	 * @returns {Promise<Object>}
	 */
    addVersionFromCopy(document, user) {
        return new Promise((resolve, reject) => {
			let result = {};

			db.models.SchemaDocumentVersion.find({
				where: {
					doc_id: document.id,
					version_number: document.versionNumber,
					subversion_number: document.subversionNumber
				}
			}).then(docVersion => {
                if (!docVersion)
                    throw new Exception('document record not found', __filename, document, 'error');

                result.id = docVersion.doc_id;

				return db.models.SchemaDocumentInstance.find({
					where: {
						doc_version_id: docVersion.id,
						lang_code: document.language
					}
				});
            }).then(docInstance => {
				if (!docInstance)
					throw new Exception('document language not found', __filename, document, 'error');

				return Promise.all([
					db.models.SchemaDocumentVersion.max('version_number', {
						where: {
							doc_id: document.id
						}
					}),
					super._copy({id: docInstance.file_id}, user)
				]);
			}).spread((currentVersion, newFileId) => {
				const hash = crypto
					.createHash('md5')
					.update(newFileId.toString() + config.settings.salt)
					.digest('hex');
				result.fileId = newFileId;
				result.url = config.http.protocol + config.http.hostname + '/file/' + hash + '/' + newFileId;

				return db.connection.transaction(t => {
					return db.models.SchemaDocumentVersion.create(
						{
							doc_id: document.id,
							version_number: currentVersion + 1,
							subversion_number: 0,
							creation_user_id: user.id
						},
						{transaction: t}
					).then(docVersion => {
						result.versionNumber = docVersion.version_number;
						result.subversionNumber = docVersion.subversion_number;
						result.language = document.language;

						return db.models.SchemaDocumentInstance.create(
							{
								doc_version_id: docVersion.id,
								lang_code: document.language,
								file_id: newFileId,
								creation_user_id: user.id
							},
							{transaction: t}
						);
					});
				});
			}).then(() => {
				resolve(result);
			}).catch(err => {
				reject(new Exception('could not create new version from copy', __filename, err, err.type || 'system'));
			})
        });
    }

    /**
     * Добавляет новую минорную версию документа
     *
     * @param {Object}              document                    Объект с данными о документе
     * @param {string}              document.id                 Идентификатор документа
     * @param {UploadableFile}      document.file               Объект с данными о загружаемом файле документа
     * @param {number}              document.versionNumber      Номер мажорной версии документа
     * @param {string}              document.language           Язык документа
     * @param {Object}              user                        Объект с данными о пользователе
     * @param {number}              user.id                     Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    addSubversion(document, user) {
        return new Promise((resolve, reject) => {
            super._upload(document.file, user).then(fileId => {
                return new Promise((resolve, reject) => {
                    const hash = crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex');
                    let result = {
                        fileId: fileId,
                        url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId
                    };

                    db.models.SchemaDocument.findByPrimary(document.id).then(doc => {
                        if (!doc)
                            throw new Exception('document record not found', __filename, document, 'error');

                        result.id = doc.id;

						return db.models.SchemaDocumentVersion.max('subversion_number', {
							where: {
								doc_id: doc.id,
								version_number: document.versionNumber
							}
						});
                    }).then(currentSubversion => {
                        return db.connection.transaction(t => {
                            return db.models.SchemaDocumentVersion.create(
                                {
                                    doc_id: document.id,
                                    version_number: document.versionNumber,
                                    subversion_number: currentSubversion + 1,
                                    creation_user_id: user.id
                                },
                                {transaction: t}
                            ).then(docVersion => {
                                result.versionNumber = docVersion.version_number;
                                result.subversionNumber = docVersion.subversion_number;
                                result.language = document.language;

                                return db.models.SchemaDocumentInstance.create(
                                    {
                                        doc_version_id: docVersion.id,
                                        lang_code: document.language,
                                        file_id: fileId,
                                        creation_user_id: user.id
                                    },
                                    {transaction: t}
                                );
                            });
                        });
                    }).then(() => {
                        resolve(result);
                    }).catch(reject);
                });
            }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(new Exception('could not add document subversion', __filename, err, err.type || 'system'));
            });
        });
    }

    /**
     * Добавляет языковую версию документа
     *
     * @param {Object}              document                        Объект с данными о документе
     * @param {string}              document.id                     Идентификатор документа
     * @param {UploadableFile}      document.file                   Объект с данными о загружаемом файле документа
     * @param {number}              document.versionNumber          Номер мажорной версии документа
     * @param {number}              document.subversionNumber       Номер минорной версии документа
     * @param {string}              document.language               Язык документа
     * @param {Object}              user                            Объект с данными о пользователе
     * @param {number}              user.id                         Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    addLanguage(document, user) {
        return new Promise((resolve, reject) => {
            super._upload(document.file, user).then(fileId => {
                return new Promise((resolve, reject) => {
                    const hash = crypto.createHash('md5').update(fileId.toString() + config.settings.salt).digest('hex');
                    let result = {
                        fileId: fileId,
                        url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + fileId
                    };

                    db.models.SchemaDocument.findByPrimary(document.id).then(doc => {
                        if (!doc)
                            throw new Exception('document record not found', __filename, document, 'error');

                        result.id = doc.id;

                        return db.models.SchemaDocumentVersion.find({
                                where: {
                                    doc_id: doc.id,
                                    version_number: document.versionNumber,
                                    subversion_number: document.subversion_number
                                }
                            });
                    }).then(docVersion => {
                        if (!docVersion)
                            throw new Exception('document version record not found', __filename, document, 'error');

                        result.versionNumber = docVersion.version_number;
                        result.subversionNumber = docVersion.subversion_number;
                        result.language = document.language;

                        return db.models.SchemaDocumentInstance.create({
                            doc_version_id: docVersion.id,
                            lang_code: document.language,
                            file_id: fileId,
                            creation_user_id: user.id
                        });
                    }).then(() => {
                        resolve(result);
                    }).catch(reject);
                });
            }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(new Exception('could not add document language', __filename, err, err.type || 'system'));
            });
        });
    }
    /**
     * Возвращает полную информацию о документе
     *
     * @param {Object}              document                        Объект с данными о документе
     * @param {number}              document.id                     Идентификатор документа
     * @param {number}              document.versionNumber          Номер мажорной версии документа
     * @param {number}              document.subversionNumber       Номер минорной версии документа
     * @param {string}              document.language               Язык документа
     * @returns {Promise<Object>}
     */
    getInfo(document) {
        return new Promise((resolve, reject) => {
            let result = {};
            db.models.SchemaDocumentVersion.find({
				where: {
					doc_id: document.id,
					version_number: document.versionNumber,
					subversion_number: document.subversionNumber
				},
				include: [{
					model: db.models.SchemaDocument,
					as: 'document',
					include: [{
						model: db.models.SchemaRefDocumentType,
						as: 'type'
					}]
				}]
			}).then(docVersion => {
                if (!docVersion)
                    throw new Exception('document record not found', __filename, document, 'error');

                result.id = docVersion.document.id;
                result.versionNumber = docVersion.version_number;
                result.subversionNumber = docVersion.subversion_number;
                result.language = document.language;
                result.type = docVersion.document.type.name;

                return db.models.SchemaDocumentInstance.find({
                    where: {
                        doc_version_id: docVersion.id,
                        lang_code: document.language
                    }
                });
            }).then(docInstance => {
                if (!docInstance)
                    throw new Exception('document language not found', __filename, document, 'error');

                return super.getInfo({id: docInstance.file_id});
            }).then(fileInfo => {
                result.file = fileInfo.pop();

                resolve(result);
            }).catch(err => {
                reject(new Exception('could not retrieve document information', __filename, err, err.type || 'system'));
            })
        });
    }

    getByType(document) {
        return new Promise((resolve,reject) => {
            db.models.SchemaRefDocumentType.find({
                where: {
                    name: document.type
                }
            }).then(type => {
                if (!type)
                    throw new Exception('type not found', __filename, document);


            })
        })
    }

    /**
     * Сохраняет подпись файла документа
     *
     * @param {Object}              document                        Объект с данными о документе
     * @param {string}              document.id                     Идентификатор документа
     * @param {number}              document.versionNumber          Номер мажорной версии документа
     * @param {number}              document.subversionNumber       Номер минорной версии документа
     * @param {string}              document.language               Язык документа
     * @param {UploadableFile}      document.signature              Подпись хэша файла документа
     * @param {Object}              user                            Объект с данными о пользователе
     * @param {number}              user.id                         Идентификатор пользователя
     * @param {number}              user.orgId                      Идентификатор организации пользователя
     * @returns {Promise<Object>}
     */
    sign(document, user) {
        return new Promise((resolve, reject) => {
            let result = {};

			db.models.SchemaDocumentVersion.find({
				where: {
					doc_id: document.id,
					version_number: document.versionNumber,
					subversion_number: document.subversionNumber
				}
			}).then(docVersion => {
                if (!docVersion)
                    throw new Exception('document record not found', __filename, document, 'error');

                result.id = document.id;
                result.versionNumber = docVersion.version_number;
                result.subversionNumber = docVersion.subversion_number;
                result.language = document.language;

                return db.models.SchemaDocumentInstance.find({
                    where: {
                        doc_version_id: docVersion.id,
                        lang_code: document.language
                    }
                });
            }).then(docInstance => {
                if (!docInstance)
                    throw new Exception('document language not found', __filename, document, 'error');

                return super.sign({id: docInstance.file_id, signature: document.signature}, user);
            }).then(() => {
                resolve(result);
            }).catch(err => {
                reject(new Exception('could not save document signature', __filename, err, err.type || 'system'));
            });
        });
    }

	/**
	 * FIXME: временные методы для миграции
	 */

	/**
	 * Возможно, этот метод ни разу и не временный, как минимум из-за одного кейса - создание документа для прикрепления
	 * к заявке в тендерном блоке из файла в "файловом хранилище" организации в личном кабинете (ибо файлы организации,
	 * по крайней мере в "файловом хранилище", представлять в виде документов смысла не имеет в принципе)
	 *
	 * Создает новый документ из существующего файла
	 *
	 * @param {Object}              document                   	Объект с данными о документе
	 * @param {Object}      		document.file              	Объект с данными о загружаемом файле документа
	 * @param {Number}				document.file.id			Идентификатор файла
	 * @param {String}              document.type              	Тип документа
	 * @param {String}              [document.language]        	Язык документа (опционально)
	 * @param {Object}              user                       	Объект с данными о пользователе
	 * @param {Number}              user.id                    	Идентификатор пользователя
	 * @returns {Promise<Object>}
	 */
	createFromFile(document, user) {
		return new Promise((resolve, reject) => {
			const hash = crypto.createHash('md5').update(document.file.id.toString() + config.settings.salt).digest('hex');
			let result = {
				fileId: document.file.id,
				url: config.http.protocol + config.http.hostname + '/file/' + hash + '/' + document.file.id,
				type: document.type
			};

			Promise.all([
				db.models.SchemaFile.findByPrimary(document.file.id),
				db.models.SchemaRefDocumentType.find({
					where: {
						name: document.type
					}
				})
			]).spread((fileRecord, typeRecord) => {
				if (!fileRecord)
					throw new Exception('no file record found', __filename, document.file, 'error');

				if (!typeRecord)
					throw new Exception('document type not found', __filename, document.type, 'error');

				return db.connection.transaction(t => {
					return db.models.SchemaDocument.create(
						{type_id: typeRecord.id},
						{transaction: t}
					).then(doc => {
						result.id = doc.id;

						return db.models.SchemaDocumentVersion.create(
							{
								doc_id: doc.id,
								version_number: 1,
								subversion_number: 0,
								creation_user_id: user.id
							},
							{transaction: t}
						);
					}).then(docVersion => {
						result.versionNumber = docVersion.version_number;
						result.subversionNumber = docVersion.subversion_number;
						result.language = document.language;

						return db.models.SchemaDocumentInstance.create(
							{
								doc_version_id: docVersion.id,
								lang_code: document.language,
								file_id: fileRecord.id,
								creation_user_id: user.id
							},
							{transaction: t}
						);
					});
				});
			}).then(() => {
				resolve(result);
			}).catch(err => {
				reject(new Exception('failed to create document from file', __filename, err, err.type || 'system'));
			});
		});
	}
}

module.exports.Document = Document;
module.exports.document = new Document();
