'use strict';

/**
 * HTTP-server
 * Отдает файлы в web-интерфейсы.
 * Работает в связке с nginx (настраивал Ваня) и скрывает реальное расположние файлов
 */

const http = require('http');
const url = require('url');
const querystring = require('querystring');
const path = require('path');
const contentDisposition = require('content-disposition');

const config = require(__dirname + '/../../config.js');
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Router = require(path.join(config.application.root, config.application.services, 'router'));

class HTTPServer {
    constructor() {
        this._server = new http.Server();
    }

    start() {
        /**
         * Callback для события request http-сервера
         * @callback HTTPServer~httpRequestCallback
         * @param {IncomingMessage} request
         * @param {ServerResponse} response
         */

        /**
         * @param {String} название события
         * @param {HTTPServer~httpRequestCallback}
         */
        this._server.on('request', (request, response) => {

            logger.info('service', 'incoming http request', request.url);

            //URL-обработчик
            /** @type {Url} */
            let requestURL = url.parse(request.url);
            logger.debug('monitor', 'incoming http URL', requestURL);

            //данные
            /** @type {ParsedQueryString} */
            let data = querystring.parse(requestURL.query);
            logger.debug('monitor', 'incoming http request data', data);

            //роутим запрос
            Router.route(requestURL.pathname.replace('/', ''), data).then(reply => {
                logger.debug('data', 'real file path', reply);

                response.setHeader('Content-Disposition', contentDisposition(reply.name, {type: 'inline'}));
                response.setHeader('X-Accel-Redirect', path.join('/upload', reply.path));
                response.end();
            }).catch(error => {
                //TODO: решить, что отдавать клиенту в случае ошибки (в частности, если файл не найден)
                logger.error(error);
                let reply = {
                    response: {},
                    error: {
                        text: error.toString(),
                        //TODO: выставлять статус на основе типа (может кода) ошибки
                        status: 404
                    }
                };
				logger.debug(reply);
				response.end(JSON.stringify(reply));
            });
        });

        //слушаем входящие запросы
        this._server.listen(config.http.port, config.http.host,
            () => logger.info('service', 'http server has been started', config.http.host + ":" + config.http.port)
        );

        //в случае ошибки
        this._server.on('error', data => logger.error('service', 'http server hasn\'t been started', data));
    }
}

module.exports = HTTPServer;