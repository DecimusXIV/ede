'use strict';

const net = require('net');
const path = require('path');
const PackageSocket = require('soa-protocol').PackageSocket;
const Message = require('soa-protocol').Message;
const Response = require('soa-protocol').Response;

const config = require(__dirname + '/../../config.js');
const Router = require(path.join(config.application.root, config.application.services, 'router'));
const logger = require(path.join(config.application.root, config.application.components, 'logger'));

/**
 * TCP-server
 * Служба отвечает за обработку входящих запросов к сервису
 */
class TCPServer {

    constructor() {
        this._server = net.Server();
    }

    start() {
        //подключение
        this._server.on('connection', socket => {

            logger.info('service', 'incoming tcp connection from', socket.remoteAddress);

            //создаем сокет
            const packageSocket = PackageSocket.create(socket);
            logger.debug('service', 'tcp socket', packageSocket);

            //получение пакета
            packageSocket.on('package', pack => {
                logger.debug('monitor', 'tcp package', pack);

                if (pack.command) {
                    //роутим на обработчик
                    Router.route(pack.command.slice(pack.command.indexOf('.') + 1), pack.data).then(result => {
                        logger.info('service', 'tcp connection reply', result);
                        //возвращаем ответ
                        packageSocket.end(
                            Response.create(
                                Message.create({
                                    name: pack.command,
                                    type: 'success',
                                    data: result
                                })
                            ).toString()
                        );
                    }).catch(error => {
                        logger.error('service', 'tcp connection reply error', error);

                        packageSocket.end(
                            Response.create(
                                Message.create({
                                    name: pack.command,
                                    type: error.type,
                                    data: {
                                        message: error.toString()
                                    }
                                })
                            ).toString()
                        );
                    });
                }

                if (pack.event) {

                }
            });
        });

        //слушаем входящие запросы
        this._server.listen(config.tcp.port, config.tcp.host,
            () => logger.info('service', 'tcp server has been started', config.tcp.host + ":" + config.tcp.port)
        );

        //в случае ошибки
        this._server.on('error', err => logger.error('service', 'tcp server hasn\'t been started', err));
    }
}

module.exports = TCPServer;
