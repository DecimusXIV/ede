'use strict';

const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');

const config = require(__dirname + '/../../config.js');
const logger = require(path.join(config.application.root, config.application.components, 'logger'));
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));

/**
 * Роутер
 * Назначет контроллер к входящим по tcp/http запросам
 */
class Router {
	/**
	 * Вызов контроллера
	 * @param {String} handle - обработчик
	 * @param {ParsedQueryString} data - данные
     * @returns {Promise}
	 */
	static route (handle, data) {
		return new Promise((resolve, reject) => {
            logger.info('service', 'controller initialization', data);

            let [controllerName, actionName] = handle.split('.');

            logger.debug('monitor', 'controller handle', controllerName);

            //собираем абсолютный путь
            let route = path.join(config.application.root, config.application.controllers, controllerName + '.js');
            logger.debug('monitor', 'controller route', route);

            //вызываем файл-обработчик, если он существует
            if (fs.existsSync(route)) {
                logger.debug('service', 'controller file exist', 'file exists', route);
                let controller = require(route);

                if (actionName in controller)
                    resolve(controller[actionName](data));
                else
                    reject(new Exception('invalid action name', __filename, actionName));
            } else {
                logger.warn('service', 'controller file exist', 'file doesn\'t exist', route);
                reject(new Exception('invalid controller name', __filename, controllerName));
            }
		});
	}
}

module.exports = Router;
