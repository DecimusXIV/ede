'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const config = require(__dirname + '/../../config.js');

//директория с моделями
const modelsDir = path.join(config.application.root, config.application.models);

/**
 * Класс для работы с БД
 * @property {Object}                   models                          - Объект, содержащий модели
 * @property {SchemaFile}               models.SchemaFile               - Модель таблицы file
 * @property {SchemaRefDocumentType}    models.SchemaRefDocumentType    - Модель таблицы ref_document_type
 * @property {SchemaRefModule}          models.SchemaRefModule          - Модель таблицы ref_module
 * @property {SchemaTemplate}           models.SchemaTemplate           - Модель таблицы template
 * @property {SchemaCertificate}        models.SchemaCertificate        - Модель таблицы certificate
 * @property {SchemaSignature}          models.SchemaSignature          - Модель таблицы signature
 * @property {SchemaDocument}           models.SchemaDocument           - Модель таблицы document
 * @property {SchemaDocumentVersion}    models.SchemaDocumentVersion    - Модель таблицы document_version
 * @property {SchemaDocumentInstance}   models.SchemaDocumentInstance   - Модель таблицы document_instance
 * @property {SchemaRefLanguage}        models.SchemaRefLanguage        - Модель таблицы ref_language
 * @property {SchemaImageResized}       models.SchemaImageResized       - Модель таблицы image_resized
 * @property {Sequelize}                connection                      - Объект соединения с БД
 * @property {Sequelize}                zakazrf                         - Объект соединения с zakazrf
 */
class Db {
    constructor() {
        //устанавливаем соединение
        let connection = new Sequelize(
            config.database.main.type + '://' + config.database.main.login + ':' + config.database.main.password
            + '@' + config.database.main.location + '/' + config.database.main.name,
            {
                logging: config.database.main.debug
            }
        );

        // TODO: пока подключение дополнительной БД будет костылем, потом можно будет сделать стандартным механизмом
        let zakazrf = new Sequelize(
            config.database.zakazrf.type + '://' + config.database.zakazrf.login + ':' + config.database.zakazrf.password
            + '@' + config.database.zakazrf.location + '/' + config.database.zakazrf.name,
            {
                logging: config.database.zakazrf.debug
            }
        );

        //считываем файлы
        fs
            .readdirSync(modelsDir)
            .filter(file => {
                return (file.indexOf('.') !== 0 && fs.lstatSync(path.join(modelsDir, file)).isFile());
            })
            .forEach(file => {
                connection.import(path.join(modelsDir, file));
            });

        // и для zakazrf
        fs
            .readdirSync(path.join(modelsDir, 'zakazrf'))
            .filter(file => {
                return (file.indexOf('.') !== 0);
            })
            .forEach(file => {
                zakazrf.import(path.join(modelsDir, 'zakazrf', file));
            });


        Object.keys(connection.models).forEach(function (modelName) {
            if ('associate' in connection.models[modelName]) {
                connection.models[modelName].associate(connection.models);
            }
        });

        this._connection = connection;
        this._zakazrf = zakazrf;
    }

    get models() {
        return this._connection.models;
    }

    get connection() {
        return this._connection;
    }

    get zakazrf() {
        return this._zakazrf;
    }
}

module.exports = new Db();