'use strict';

const moment = require('moment');
const config = require(__dirname + '/../../config.js');

const levels = {
	'debug'		: 0,
	'info'		: 1,
	'warning'	: 2,
	'error'		: 3
};

class Logger {
	constructor(level) {
		if (levels.hasOwnProperty(level)) {
			this._level = levels[level];
		} else {
            console.log(`[${moment().format('YYYY-MM-DD hh:mm:ss.ms')}] <WARN> could not init logger with level ` +
				`${level}, level set to 'info'`);
            this._level = 1;
		}
	}

	debug() {
		if (this._level === 0) {
			console.log(`[${moment().format('YYYY-MM-DD hh:mm:ss.ms')}] <DEBUG>`, ...arguments);
		}
	}

    info() {
        if (this._level <= 1) {
            console.log(`[${moment().format('YYYY-MM-DD hh:mm:ss.ms')}] <INFO>`, ...arguments);
        }
    }

    warn() {
        if (this._level <= 2) {
            console.log(`[${moment().format('YYYY-MM-DD hh:mm:ss.ms')}] <WARN>`, ...arguments);
        }
    }

    error() {
        if (this._level <= 3) {
            console.log(`[${moment().format('YYYY-MM-DD hh:mm:ss.ms')}] <ERROR>`, ...arguments);
        }
    }
    /**
     * Логирует данные сервисов/входящих и исходящих данных/внутренние данные логики (см config.js)
     * @deprecated
     * @param type (service/data/monitor)
     * @param description
     * @param data
     * @param file
     */
	static log(type, description, data, file) {
		//определяем установлен ли запрашиваемый тип дебага в конфиге
		if (config.debug[type]) {
			let message = '';
			switch (type) {
				case 'service':
					message =
						'fileserver service debug message:\n' + description + ' with data {' + JSON.stringify(data) + '}\n(file:' +
						' ' + file + ')\n';
					break;
				case 'data':
					message =
						'fileserver data debug message:\n' + description + ' with data {' + JSON.stringify(data) + '}\n(file: ' + file + ')\n';
					break;
				case 'monitor':
					message =
						'fileserver monitor debug message:\n' + description + ' with data {' + JSON.stringify(data) + '}\n(file:' +
						' ' + file + ')\n';
					break;
				default:
					message =
						'unknown debug type';
			}
			console.log(message);
		}
	}
}

module.exports = new Logger(config.debug.level);