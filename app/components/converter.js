const TEN_NAMES = ['', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
const HUNDRED_NAMES = ['сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
const DIGIT_NAMES = [['один', 'одна'], ['два', 'две'], 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'];
const FROM_10_TO_20_NAMES = ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];

/**
 * Подключаем библиотеку для проерки типов
 */
const util = require('util');

/**
 * Возвращает форму слова, соответствующую числу
 * @param value число
 * @param forms массив из 3 форм слова, например ['рубль', 'рубля', 'рублей']
 */
function morph (value, forms) {
	if (!util.isArray(forms)) {
		throw Error('Second parameter must be array');
	}
	if (forms.length < 3) {
		throw Error('Second parameter must have three items');
	}

	value = '' + value;

	if (/1\d$/.test(value)) {
		return forms[2];
	}
	else if (/1$/.test(value)) {
		return forms[0];
	}
	else if (/(2|3|4)$/.test(value)) {
		return forms[1];
	}
	else {
		return forms[2];
	}
}

/**
 * Преобразаует число меньше тысячи в строку
 * Вспомогательная функция
 * @param number
 * @param woman_name
 * @returns string
 */
function toString (number, woman_name) {
	var value = Number(number) || 0,
		hundred = Math.floor(value / 100),
		ten = Math.floor((value % 100) / 10),
		digit = value % 10,
		result = [];

	if (hundred) {
		result.push(HUNDRED_NAMES[hundred - 1]);
	}

	if (ten === 1) {
		result.push(FROM_10_TO_20_NAMES[digit]);
	} else if(ten > 1) {
		result.push(TEN_NAMES[ten - 1]);
	}

	if (digit && ten !== 1) {
		if (typeof (DIGIT_NAMES[digit - 1]) === "object") {
			result.push(DIGIT_NAMES[digit - 1][woman_name ? 1 : 0]);
		} else {
			result.push(DIGIT_NAMES[digit - 1]);
		}
	}

	return result.join(' ');
}

/**
 * Преобразует число в строку
 * @param value число
 * @returns string строковое представление числа
 */
function numberToString (value) {
	var rankConfig = [
			['', '', ''],
			['тысяча', 'тысячи', 'тысяч'],
			['миллион', 'миллиона', 'миллионов'],
			['миллиард', 'миллиарда', 'миллиардов'],
			['триллион', 'триллиона', 'триллионов']
		],
		result = [],
		preparedNumbers = processNumber(value);

	for (var i = 0; i < preparedNumbers.length; i++) {
		if (preparedNumbers[i][2] > 0) {
			result.unshift(morph(preparedNumbers[i][0], rankConfig[preparedNumbers[i][2]]));
		}
		result.unshift(preparedNumbers[i][1]);
	}

	return result.join(' ');
}

/**
 * Обрабатывает входящее число
 * Возвращает массив, каждый элемент которого содержит число, не превосходящее 1000, и его текстовое представление
 * @param number
 * @returns {Array}
 */
function processNumber (number) {
	var rank = 0,
		value = Number(number) || 0,
		result = [],
		remainder;

	do {
		remainder = value % 1000;
		if (remainder) {
			result.push([remainder, toString(remainder, rank === 1), rank]);
		}

		rank++;
		value = Math.floor(value / 1000);
	} while (value > 0);

	return result;
}

/**
 * Преобразует число в текстовое представление в бухгалтерской форме
 * @param sum Число для преобразования
 * @returns string Текстовое представление
 */
function moneyToString (sum) {
	var number = Number(sum) || 0,
		rubleReplacements = ['рубль', 'рубля', 'рублей'],
		coinReplacements = ['копейка', 'копейки', 'копеек'],
		splitNumber,
		rubleNumber,
		coinNumber,
		result = [];

	if (number < 0) {
		throw Error('Wrong sum value');
	}

	splitNumber = number.toFixed(2).split('.');

	rubleNumber = Number(splitNumber[0]) || 0;
	coinNumber = Number(splitNumber[1]) || 0;

	if (rubleNumber) {
		result.push(
			numberToString(rubleNumber),
			morph(rubleNumber, rubleReplacements)
		);
	}

	result.push(
		(coinNumber < 10 ? '0' : '') + coinNumber,
		morph(coinNumber, coinReplacements)
	);

	return result.join(' ');
}

module.exports = {
	moneyToString: moneyToString,
	morph: morph
};