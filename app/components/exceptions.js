'use strict';

class Exception extends Error {
    constructor(message, file, context, type) {
        super();

        this._message = message;
        this._file = file;
        this._context = context;

        // по конвенции ответа сервиса: error - ошибка логики, system - не связанная с логикой
        this._type = type || 'system';
    }

    get message() {
        return this._message;
    }

    get context() {
        return this._context;
    }

    get type() {
        return this._type;
    }

    toString() {
        const additionalMessage = ('object' === typeof this._context && 'message' in this._context)
            ? this._context.message
            : this._context;

        return this._message + ': ' + additionalMessage;
    }
}

module.exports = Exception;

