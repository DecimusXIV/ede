'use strict';

const config = require('../../config.js');
const path = require('path');
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const db = require(path.join(config.application.root, config.application.components, 'db'));
const Command = require('soa-protocol').Command;
const Connector = require('soa-protocol').Connector;
const x509 = require('x509');
const fs = require('fs');
const Promise = require('bluebird');
const mkdirp = require('mkdirp');

/**
 * Получение сертификата или из файла, или из сервиса пользователей
 *
 * @param {Number}   userId    Идентификатор пользователя
 * @returns {Promise<String>}
 */
function getCertificatePath(userId) {
    return new Promise((resolve, reject) => {
        db.models.SchemaCertificate.find({
            where: {
                user_id: userId
            }
        }).then(result => {
            if (!result || !fs.existsSync(path.join(config.application.cert_store, result.cer_path))) {
                return new Promise((resolve, reject) => {
                    let connector = new Connector(4400);

                    connector.on('connect', () => {
                        connector.write(Command.create('user.User.certificate', userId).toString());
                    });

                    connector.on('package', pkg => {
                        connector.disconnect();
                        if (pkg.isSuccess()) {
                            saveCertificate(userId, pkg.getData().certificate)
                                .then(cer_path => {
                                    return resolve(cer_path);
                                })
                                .catch(err => {
                                    return reject(err);
                                });
                        } else {
                            return reject(
                                new Exception('error communicating with users service', __filename, pkg.getMessage(), 'system')
                            );
                        }
                    });

                    connector.on('error', err => {
                        return reject(new Exception('error communicating with users service', __filename, err, 'system'));
                    });

                    connector.connect();
                });
            } else {
                return path.join(config.application.cert_store, result.cer_path);
            }
        }).then(cer_path => {
            resolve(cer_path);
        }).catch(err => {
            reject(new Exception('could not retrieve user certificate path', __filename, err))
        });
    });
}

/**
 * Сохранение файла сертификата в файловой системе и запись пути в базу
 *
 * @param {Number}     userId      Идентификатор пользователя
 * @param {String}     cert        Сертификат в формате PEM
 * @returns {Promise<String>}
 */
function saveCertificate(userId, cert) {
    return new Promise((resolve, reject) => {
        let filename = x509.parseCert(cert).serial + '.'
            + new Date().toISOString().replace(/ |:|T|Z|\.|-|\d{3}(?=Z)/g, '') + '.cer';
        let filePath = path.join(filename.substring(0, 3), filename);
        let dir = path.join(config.application.cert_store, filename.substring(0, 3));
        let filePathFull = path.join(dir, filename);

        mkdirp(dir, err => {
            if (err) {
                if (err.code === 'EEXIST') {
                    // perfectly okay
                } else {
                    return reject(new Exception('could not create a directory for certificate', __filename, err));
                }
            }
            fs.writeFile(filePathFull, cert, err => {
                if (err)
                    return reject(new Exception('could not write certificate file', __filename, err));

                db.models.SchemaCertificate.upsert({
                    user_id: userId,
                    cer_path: filePath
                }).then(() => {
                    return resolve(filePathFull);
                }).catch(err => {
                    return reject(new Exception('could not save certificate to database', __filename, err));
                });
            });
        });
    });
}

module.exports.getCertificatePath = getCertificatePath;
module.exports.saveCertificate = saveCertificate;