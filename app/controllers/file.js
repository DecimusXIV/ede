'use strict';

const path = require('path');
const Promise = require('bluebird');
const cryptopro = require('cryptopro');

const config = require(__dirname + '/../../config.js');
const fileValidator = require(path.join(config.application.root, config.application.validators, 'file')).fileValidator;
const fileProcessor = require(path.join(config.application.root, config.application.processors, 'file')).file;

class FileController {
    /**
     * Загрузить файл(ы) синхронно
     *
     * @param {Object}                              data        Объект с данными о файле и пользователе
     * @param {UploadableFile|UploadableFile[]}     data.file   Объект с данными о файле(ах)
     * @param {Object}                              data.user   Объект с данными о пользователе
     * @returns {Promise}
     */
    //TODO:проверка прав
    static upload(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateUpload(data)
                .then(validated => {
                    resolve(fileProcessor.upload(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Загрузить файл(ы) асинхронно
     *
     * @param {Object}                            data        Объект с данными о файле и пользователе
     * @param {UploadableFile|UploadableFile[]}   data.file   Объект с данными о файле(ах)
     * @param {Object}                            data.user   Объект с данными о пользователе
     * @returns {Promise}
     */
    //TODO:проверка прав
    static uploadAsync(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateUpload(data)
                .then((validated) => {
                    resolve(fileProcessor.uploadAsync(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Возвращает информацию о файле
     *
     * @param {Object}  data        Объект с данными о файле
     * @param {Object}  data.file   Объект с данными о файле
     * @returns {Promise}
     */
    //TODO: проверка прав
    static getInfo(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateGetInfo(data)
                .then((validated) => {
                    resolve(fileProcessor.getInfo(validated.file));
                })
                .catch(reject);
        });
    }

    /**
     * Сохранить подпись хэша файла
     *
     * @param {Object}  data        Объект с данными о пользователе и файле
     * @param {Object}  data.user   Объект с данными о пользователе
     * @param {Object}  data.file   Объект с данными о файле
     * @returns {Promise}
     */
    //TODO: проверка прав
    static sign(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateSign(data)
                .then(validated => {
                    resolve(fileProcessor.sign(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Пометить файл на удаление
     *
     * @param {Object}                  data          Объект с данными запроса
     * @param {Object}                  data.file     Объект с данными о файле
     * @param {Array<Number>|Number}    data.file.id  Идентификатор файла
     * @param {Object}                  data.user     Объект с данными о пользователе
     * @param {Number}                  data.user.id  Идентификатор пользователя
     * @returns {Promise}
     */
    //TODO: проверка прав
    static delete(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateDelete(data)
                .then(validated => {
                    resolve(fileProcessor.markDeleted(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Возвращает путь к файлу для nginx
     *
     * @param {Object} data
     * @returns {Promise}
     */
    //TODO: проверка прав
    static download(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateDownload(data)
                .then(validated => {
                    resolve(fileProcessor.getPath(validated));
                })
                .catch(reject);
        });
    }

    /**
     * Возвращает хэш файла, вычисленный CryptoPro
     *
     * @param {Object} data Объект с данными запроса
     * @returns {Promise}
     */
    static computeHash(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateComputeHash(data)
                .then(validated => {
                    resolve(cryptopro.getFileHash(validated.file.path));
                })
                .catch(reject);
        });
    }

    /**
     * Проверка корректности подписи файла пользователем
     *
     * @param {Object}  data    Объект с данными запроса
     * @returns {Promise}
     */
    static verifySignature(data) {
        return new Promise((resolve, reject) => {
            fileValidator.validateVerifySignature(data)
                .then(validated => {
                    resolve(fileProcessor.verifySignature(validated.file, validated.user));
                })
                .catch(reject);
        });
    }
}

module.exports = FileController;