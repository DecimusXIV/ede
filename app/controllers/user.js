'use strict';

const Promise = require('bluebird');
const path = require('path');

const config = require(__dirname + '/../../config.js');
const certificates  = require(path.join(config.application.root, config.application.components, 'certificates'));

class UserController {
    /**
     * Это реакция на событие от сервиса пользователей о добавлении нового сертификата для пользователя
     *
     * @param {Object} data
     * @returns {Promise}
     */
    static certificateAdded(data) {
        return new Promise((resolve, reject) => {
            certificates.saveCertificate(data.userId, data.cert)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }
}

module.exports = UserController;