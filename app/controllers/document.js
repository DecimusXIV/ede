'use strict';

const path = require('path');
const Promise = require('bluebird');

const config = require(__dirname + '/../../config.js');
const documentValidator = require(path.join(config.application.root, config.application.validators, 'document'));
const documentProcessor = require(path.join(config.application.root, config.application.processors, 'document')).document;

class DocumentController {
    /**
     * Загрузить документ
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    //TODO:проверка прав
    static upload(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateUpload(data)
                .then(validated => {
                    resolve(documentProcessor.upload(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Добавить мажорную версию
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    static addVersion(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateAddVersion(data)
                .then(validated => {
                    resolve(documentProcessor.addVersion(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

	/**
	 * Создает новую мажорную версию документа путем копирования существующей версии
	 *
	 * @param {Object}              data                       			Объект с данными запроса
	 * @param {Object}              data.document              			Объект с данными копируемого документа
	 * @param {Object}              data.user                  			Объект с данными о пользователе
	 * @returns {Promise<Object>}
	 */
	static addVersionFromCopy(data) {
		return new Promise((resolve, reject) => {
			documentValidator.validateAddVersionFromCopy(data)
				.then(validated => {
					resolve(documentProcessor.addVersionFromCopy(validated.document, validated.user));
				})
				.catch(reject);
		});
	}

    /**
     * Добавить минорную версию
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    static addSubversion(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateAddSubversion(data)
                .then(validated => {
                    resolve(documentProcessor.addSubversion(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Добавить языковую версию
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    static addLanguage(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateAddLanguage(data)
                .then(validated => {
                    resolve(documentProcessor.addLanguage(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Сохранить подпись документа
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    static sign(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateSign(data)
                .then(validated => {
                    resolve(documentProcessor.sign(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Получить информацию о документе
     *
     * @param {Object}   data            Объект с данными запроса
     * @param {Object}   data.document   Объект с данными о документе
     * @param {Object}   data.user       Объект с данными о пользователе
     * @returns {Promise}
     */
    static getInfo(data) {
        return new Promise((resolve, reject) => {
            documentValidator.validateGetInfo(data)
                .then(validated => {
                    resolve(documentProcessor.getInfo(validated.document, validated.user));
                })
                .catch(reject);
        });
    }

	/**
	 * Создает новый документ из существующего файла
	 *
	 * @param {Object}              data                            Объект с данными запроса
	 * @param {Object}              data.document                   Объект с данными о документе
	 * @param {Object}              data.user                       Объект с данными о пользователе
	 * @returns {Promise}
	 */
    static createFromFile(data) {
    	return new Promise((resolve, reject) => {
    		documentValidator.validateCreateFromFile(data)
				.then(validated => {
					resolve(documentProcessor.createFromFile(validated.document, validated.user));
				})
				.catch(reject);
		});
	}
}

module.exports = DocumentController;