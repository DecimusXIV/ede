'use strict';

const path = require('path');
const Promise = require('bluebird');

const config = require(__dirname + '/../../config.js');
const imageValidator = require(path.join(config.application.root, config.application.validators, 'image'));
const imageProcessor = require(path.join(config.application.root, config.application.processors, 'image')).image;

class ImageController {
    /**
     * Загрузить изображения синхронно
     *
     * @param {Object}                              data        Объект с данными об изображениях и пользователе
     * @param {UploadableImage|UploadableImage[]}   data.file   Объект с данными об изображениях
     * @param {Object}                              data.user   Объект с данными о пользователе
     * @returns {Promise}
     */
    //TODO:проверка прав
    static upload(data) {
        return new Promise((resolve, reject) => {
            imageValidator.validateUpload(data)
                .then((validated) => {
                    resolve(imageProcessor.upload(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Загрузить изображения асинхронно
     *
     * @param {Object}                              data        Объект с данными об изображениях и пользователе
     * @param {UploadableImage|UploadableImage[]}   data.file   Объект с данными об изображениях
     * @param {Object}                              data.user   Объект с данными о пользователе
     * @returns {Promise}
     */
    //TODO:проверка прав
    static uploadAsync(data) {
        return new Promise((resolve, reject) => {
            imageValidator.validateUpload(data)
                .then((validated) => {
                    resolve(imageProcessor.uploadAsync(validated.file, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Получить изображения указанных размеров из исходных изображений
     *
     * @param {Object}                 data                     Объект с данными запроса
     * @param {Array<Object>|Object}   data.image               Объект с идентификатором файла и размерами или массив таковых
     * @param {Number}                 data.image.id            Идентификатор файла изображения
     * @param {Array<Number>}          data.image.dimensions    Массив размеров изображения (ширина, высота) или массив таковых
     * @returns {Promise<Array>}
     */
    static getResized(data) {
        return new Promise((resolve, reject) => {
            imageValidator.validateGetResized(data)
                .then(validated => {
                    resolve(imageProcessor.getResized(validated.image));
                })
                .catch(reject);
        });
    }
}

module.exports = ImageController;