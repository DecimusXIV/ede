'use strict';

const path = require('path');
const Promise = require('bluebird');

const config = require(__dirname + '/../../config.js');
const pdfValidator = require(path.join(config.application.root, config.application.validators, 'pdf')).pdfValidator;
const pdfProcessor = require(path.join(config.application.root, config.application.processors, 'pdf')).pdf;

class PdfController {
    /**
     * Создать файл из шаблона с указанными данными
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.data           Объект с данными для генерирования файла из шаблона
     * @param {Object}              data.template       Объект с данными шаблона
     * @param {Number}              data.template.id    Идентифиатор шаблона
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    //TODO:проверка прав
    static create(data) {
        return new Promise((resolve, reject) => {
            pdfValidator.validateCreate(data)
                .then(validated => {
                    resolve(pdfProcessor.create(validated.data, validated.template, validated.user));
                })
                .catch(reject);
        });
    }

    /**
     * Подготавливает временный файл для подписания
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.file           Объект с данными о файле
     * @param {Object}              data.file.id        Идентификатор файла
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns Promise<Promise>
     */
    static prepareSignableFile(data) {
        return new Promise((resolve, reject) => {
           pdfValidator.validatePrepareSignableFile(data)
               .then(validated => {
                   resolve(pdfProcessor.prepareSignableFile(validated.file, validated.user));
               })
               .catch(reject);
        });
    }

    /**
     * Сохраняет подпись в файл
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.file           Объект с данными о файле
     * @param {Object}              data.file.id        Идентификатор файла
     * @param {String}              data.file.signature Подпись файла в формате base64
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns {Promise<Promise>}
     */
    static saveSignature(data) {
        return new Promise((resolve, reject) => {
            pdfValidator.validateSaveSignature(data)
                .then(validated => {
                    resolve(pdfProcessor.saveSignature(validated.file, validated.user));
                })
                .catch(reject);
        })
    }

    /**
     * Возвращает путь к временному файлу, подготовленному для подписания nginx
     *
     * @param {Object}  data        Объект с данными запроса
     * @param {Number}  data.id     Идентификатор файла
     * @param {String}  data.hash   Хэш для проверки валидности запроса
     * @returns {Promise<Promise>}
     */
    static downloadTmp(data) {
        return new Promise((resolve, reject) => {
            pdfValidator.validateDownloadTmp(data)
                .then(validated => {
                    resolve(pdfProcessor.getTmpPath(validated));
                })
                .catch(reject);
        })
    }
}

module.exports = PdfController;
