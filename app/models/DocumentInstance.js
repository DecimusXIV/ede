'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaDocumentInstance
	 */
	let model = sequelize.define('SchemaDocumentInstance',
		{
			doc_version_id          : {type: DataTypes.INTEGER.UNSIGNED},
			lang_code           	: {type: DataTypes.CHAR(2), validate: {isAlpha: true}},
			file_id          		: {type: DataTypes.INTEGER.UNSIGNED},
			creation_user_id        : {type: DataTypes.INTEGER.UNSIGNED, allowNull: false},
			creation_timestamp      : {type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW},
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'document_instance',
			indexes: [{
                unique: true,
				fields: ['doc_version_id', 'lang_code', 'file_id']
            }],
			classMethods: {
				associate: models => {
                    models.SchemaDocumentInstance.belongsTo(
                        models.SchemaDocumentVersion, {
                            foreignKey: 'doc_version_id',
                            as: 'documentVersion'
                        }
                    );
                    models.SchemaDocumentInstance.belongsTo(
                        models.SchemaRefLanguage, {
                        	targetKey: 'code',
                            foreignKey: 'lang_code',
                            as: 'language'
                        }
                    );
                    models.SchemaDocumentInstance.belongsTo(
                        models.SchemaFile, {
                            foreignKey: 'file_id',
                            as: 'file'
                        }
                    );
                }
			}
		}
	);
	model.removeAttribute('id');
	return model;
};