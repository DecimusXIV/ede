'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaDocument
	 */
	return sequelize.define('SchemaImageResized',
		{
			original_file_id        : {type: DataTypes.INTEGER, allowNull: false, primaryKey: true},
			height					: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true},
			width					: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true},
            file_id                 : {type: DataTypes.INTEGER, allowNull: false, unique: true}
        },
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'image_resized',
			classMethods: {
				associate: models => {
                    models.SchemaImageResized.belongsTo(
                        models.SchemaFile, {
                            foreignKey: 'file_id',
                            as: 'file'
                        }
                    );
                    models.SchemaImageResized.belongsTo(
                        models.SchemaFile, {
                            foreignKey: 'original_file_id',
							as: 'originalFile'
                        }
                    );
                }
			}
		}
	);
};