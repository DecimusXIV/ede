'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaDocumentVersion
	 */
	return sequelize.define('SchemaDocumentVersion',
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			doc_id           		: {type: DataTypes.INTEGER, allowNull: false},
			version_number          : {type: DataTypes.INTEGER, allowNull: false},
			subversion_number       : {type: DataTypes.INTEGER, allowNull: false},
			creation_user_id        : {type: DataTypes.INTEGER, allowNull: false},
			creation_timestamp      : {type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW},
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'document_version',
			indexes: [{
                unique: true,
				fields: ['doc_id', 'version_number', 'subversion_number']
            }],
			classMethods: {
				associate: models => {
                    models.SchemaDocumentVersion.belongsTo(
                        models.SchemaDocument, {
                            foreignKey: 'doc_id',
                            as: 'document'
                        }
                    );
                    models.SchemaDocumentVersion.hasMany(
                    	models.SchemaDocumentInstance, {
                    		foreignKey: 'doc_version_id',
							as: 'instances'
						}
					);
                }
			}
		}
	);
};