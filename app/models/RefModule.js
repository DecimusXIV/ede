'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaRefModule
	 */
	return sequelize.define("SchemaRefModule",
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			code                    : {type: DataTypes.STRING(30), allowNull: false},
			name                    : {type: DataTypes.STRING, allowNull: false},
			title                   : {type: DataTypes.STRING, allowNull: false}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'ref_module',
			classMethods: {
				associate: models => {
					models.SchemaRefModule.hasMany(
						models.SchemaTemplate, {
							foreignKey: 'module_id',
							as: 'templates'
						}
					);
					models.SchemaRefModule.hasMany(
						models.SchemaFile, {
							foreignKey: 'module_id',
							as: 'files'
						}
					);
				}
			}
		});
};