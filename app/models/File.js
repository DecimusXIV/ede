'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaFile
	 */
	return sequelize.define('SchemaFile',
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			original_name           : {type: DataTypes.STRING, allowNull: false},
			size                    : {type: DataTypes.INTEGER, allowNull: false},
			mime_type               : {type: DataTypes.STRING, allowNull: false},
			module_id               : {type: DataTypes.INTEGER, allowNull: false},
			path                    : {type: DataTypes.STRING, allowNull: false},
			hash                    : {type: DataTypes.STRING(50), allowNull: false},
			creation_user_id        : {type: DataTypes.INTEGER, allowNull: false},
			creation_timestamp      : {type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW},
			cancellation_user_id    : {type: DataTypes.INTEGER},
			cancellation_timestamp  : {type: DataTypes.DATE}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'file',
			classMethods: {
				associate: models => {
                    models.SchemaFile.belongsTo(
                        models.SchemaRefModule, {
                            foreignKey: 'module_id',
                            as: 'module'
                        }
                    );
                    models.SchemaFile.hasMany(
                    	models.SchemaSignature, {
                    		foreignKey: 'file_id',
							as: 'signatures'
						}
					);
                    models.SchemaFile.hasMany(
                    	models.SchemaImageResized, {
                    		foreignKey: 'original_file_id',
							as: 'resizedImages'
						}
					);
                    models.SchemaFile.hasOne(
                    	models.SchemaImageResized, {
                    		foreignKey: 'file_id',
							as: 'imageProps'
						}
					)
                }
			}
		}
	);
};