'use strict';

module.exports = function(sequelize, DataTypes) {
    /**
     * @typedef {Model} SchemaBFile
     */
    return sequelize.define('SchemaBFile',
        {
            ID                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
            TIMESTAMP_X             : {type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW},
            MODULE_ID               : {type: DataTypes.STRING(50)},
            HEIGHT                  : {type: DataTypes.INTEGER(18)},
            WIDTH                   : {type: DataTypes.INTEGER(18)},
            FILE_SIZE               : {type: DataTypes.INTEGER(18), allowNull: false},
            CONTENT_TYPE            : {type: DataTypes.STRING},
            SUBDIR                  : {type: DataTypes.STRING},
            FILE_NAME               : {type: DataTypes.STRING, allowNull: false},
            ORIGINAL_NAME           : {type: DataTypes.STRING},
            DESCRIPTION             : {type: DataTypes.STRING},
            HANDLER_ID              : {type: DataTypes.STRING(50)}
        },
        {
            freezeTableName: true,
            timestamps: false,
            tableName: 'b_file'
        }
    );
};