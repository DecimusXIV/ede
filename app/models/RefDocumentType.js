'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaRefDocumentType
	 */
	return sequelize.define("SchemaRefDocumentType",
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			name                    : {type: DataTypes.STRING(100), allowNull: false, unique: true},
			title		            : {type: DataTypes.STRING, allowNull: false},
			description	            : {type: DataTypes.TEXT},
			order               	: {type: DataTypes.INTEGER, allowNull: false, defaultValue: 0}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'ref_document_type',
			classMethods: {
                associate: models => {
                    models.SchemaRefDocumentType.hasMany(
                        models.SchemaDocument, {
                            foreignKey: 'type_id',
							as: 'documents'
                        }
                    );
                }
			}
		});
};