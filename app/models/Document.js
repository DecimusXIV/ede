'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaDocument
	 */
	return sequelize.define('SchemaDocument',
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			type_id           		: {type: DataTypes.INTEGER, allowNull: false},
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'document',
			classMethods: {
				associate: models => {
                    models.SchemaDocument.belongsTo(
                        models.SchemaRefDocumentType, {
                            foreignKey: 'type_id',
                            as: 'type'
                        }
                    );
                    models.SchemaDocument.hasMany(
                        models.SchemaDocumentVersion, {
                            foreignKey: 'doc_id',
							as: 'Versions'
                        }
                    );
                }
			}
		}
	);
};