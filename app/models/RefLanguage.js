'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaRefLanguage
	 */
	return sequelize.define("SchemaRefLanguage",
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			code                    : {type: DataTypes.STRING(2), allowNull: false, unique: true},
			title                   : {type: DataTypes.STRING, allowNull: false}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'ref_language',
			classMethods: {
				associate: models => {
					models.SchemaRefLanguage.hasMany(
						models.SchemaDocumentInstance, {
							foreignKey: 'lang_id',
							as: 'documentInstances'
						}
					);
				}
			}
		});
};