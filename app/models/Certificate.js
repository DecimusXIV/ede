'use strict';

module.exports = function(sequelize, DataTypes) {
    /**
     * @typedef {Model} SchemaCertificate
     */
    return sequelize.define('SchemaCertificate',
        {
            user_id         : {type: DataTypes.INTEGER, primaryKey: true},
            cer_path        : {type: DataTypes.STRING, allowNull: false}
        },
        {
            freezeTableName: true,
            timestamps: false,
            tableName: 'certificate'
        }
    );
};