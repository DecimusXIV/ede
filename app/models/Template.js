"use strict";

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaTemplate
	 */
	return sequelize.define("SchemaTemplate",
		{
			id                      : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
			name                    : {type: DataTypes.TEXT(50), allowNull: false},
			portrait                : {type: DataTypes.BOOLEAN, allowNull: false},
			module_id               : {type: DataTypes.INTEGER, allowNull: false},
			creation_timestamp      : {type: DataTypes.DATE, default: DataTypes.NOW}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'template',
			classMethods: {
				associate: models => {
					models.SchemaTemplate.belongsTo(
						models.SchemaRefModule, {
							foreignKey: 'module_id',
							as: 'module'
						}
					)
				}
			}
		});
};