'use strict';

module.exports = function(sequelize, DataTypes) {
	/**
	 * @typedef {Model} SchemaSignature
	 */
	return sequelize.define('SchemaSignature',
		{
			fileId                 	: {type: DataTypes.INTEGER, field: 'file_id', primaryKey: true},
			orgId					: {type: DataTypes.INTEGER, field: 'org_id', primaryKey: true},
			userId					: {type: DataTypes.INTEGER, field: 'user_id', allowNull: false},
			orgName                	: {type: DataTypes.TEXT, field: 'org_name'},
			position                : {type: DataTypes.STRING},
			name                    : {type: DataTypes.STRING},
		    validTill              	: {type: DataTypes.DATE, field: 'valid_till'},
			signature				: {type: DataTypes.TEXT, allowNull: false},
			signTimestamp			: {
				type: DataTypes.DATE,
				field: 'sign_timestamp',
				allowNull: false,
				defaultValue: DataTypes.NOW
			}
		},
		{
			freezeTableName: true,
			timestamps: false,
			tableName: 'signature',
			classMethods: {
				associate: models =>
                    models.SchemaSignature.belongsTo(
						models.SchemaFile,
						{
							foreignKey: 'file_id',
							as: 'file'
						}
					)
			}
		}
	);
};