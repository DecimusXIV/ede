'use strict';

const path = require('path');
const Joi = require('joi');
const crypto = require("crypto");

const config = require('../../config.js');
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));

class PdfValidator {
    /**
     * Валидирует данные для экшона create
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.data           Объект с данными для генерирования файла из шаблона
     * @param {Object}              data.template       Объект с данными шаблона
     * @param {Number}              data.template.name  Название шаблона
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateCreate(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                data: Joi.object().required(),
                template: Joi.object().keys({
                    name: Joi.string().required()
                }).required(),
                user: Joi.object().keys({
                    id: Joi.number().integer().positive().required()
                }).required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона prepareSignableFile
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.file           Объект с данными о файле
     * @param {Object}              data.file.id        Идентификатор файла
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns {Promise}
     */
    validatePrepareSignableFile(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.number().integer().positive().required()
                }).required(),
                user: Joi.object().keys({
                    id: Joi.number().integer().positive().required()
                }).required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона saveSignature
     *
     * @param {Object}              data                Объект с данными запроса
     * @param {Object}              data.file           Объект с данными о файле
     * @param {Object}              data.file.id        Идентификатор файла
     * @param {String}              data.file.signature Подпись файла в формате base64
     * @param {Object}              data.user           Объект с данными о пользователе
     * @param {Number}              data.user.id        Идентификатор пользователя
     * @returns {Promise}
     */
    validateSaveSignature(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.number().integer().positive().required(),
                    signature: Joi.string().base64().required()
                }).required(),
                user: Joi.object().keys({
                    id: Joi.number().integer().positive().required()
                }).required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона download
     *
     * @param {Object}  data        Объект с данными запроса
     * @param {Number}  data.id     Идентификатор файла
     * @param {String}  data.hash   Хэш для проверки валидности запроса
     * @returns {Promise<Object>}
     */
    validateDownloadTmp(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                id: Joi.number().integer().positive().required(),
                hash: Joi.any()
                    .equal(crypto.createHash('md5').update(data.id.toString() + 'pdftmp' + config.settings.salt).digest('hex'))
                    .error(() => 'hash is invalid')
                    .required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }
}

module.exports.pdfValidator = new PdfValidator();
module.exports.PdfValidator = PdfValidator;