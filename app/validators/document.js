'use strict';

const path = require('path');
const Promise = require('bluebird');
const Joi = Promise.promisifyAll(require('joi'));

const config = require('../../config.js');
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const FileValidator = require(path.join(config.application.root, config.application.validators, 'file')).FileValidator;
const db = require(path.join(config.application.root, config.application.components, 'db'));

class DocumentValidator extends FileValidator {
    constructor() {
        super();
    }

    /**
     * Валидирует данные для экшонов upload/uploadAsync
     *
     * @param {Object}              data                            Объект с данными запроса
     * @param {Object}              data.document                   Объект с данными о документе
     * @param {UploadableFile}      data.document.file              Объект с данными о загружаемом файле документа
     * @param {String}              data.document.type              Тип документа
     * @param {String}              [data.document.language]        Язык документа (опционально)
     * @param {Object}              data.user                       Объект с данными о пользователе
     * @param {Number}              data.user.id                    Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateUpload(data) {
        return new Promise((resolve, reject) => {
            let dataSchema = Joi.object().keys({
                user: this.userSchema.required(),
                document: Joi.object().keys({
                    type: Joi.string().max(100).required(),
                    language: Joi.string().length(2).default('ru'),
                    file: this.uploadFileSchema.required()
                }).required(),
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err));

                super._validateFileAdditional(value.document.file)
                    .then(() => {
                        return resolve(value);
                    })
                    .catch(err => {
                        return reject(new Exception('validation failed', __filename, err));
                    })
            });
        });
    }

    /**
     * Валидирует данные для экшона addVersion
     *
     * @param {Object}              data                       Объект с данными запроса
     * @param {Object}              data.document              Объект с данными о документе
     * @param {String}              data.document.id           Идентификатор документа
     * @param {UploadableFile}      data.document.file         Объект с данными о загружаемом файле документа
     * @param {String}              [data.document.language]   Язык документа (опционально)
     * @param {Object}              data.user                  Объект с данными о пользователе
     * @param {Number}              data.user.id               Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateAddVersion(data) {
        return new Promise((resolve, reject) => {
            let dataSchema = Joi.object().keys({
                user: this.userSchema.required(),
                document: Joi.object().keys({
                    id: Joi.number().integer().positive().required(),
                    file: this.uploadFileSchema.required(),
                    language: Joi.string().length(2).default('ru')
                }).required()
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err));

                super._validateFileAdditional(value.document.file)
                    .then(() => {
                        return resolve(value);
                    })
                    .catch(err => {
                        return reject(new Exception('validation failed', __filename, err));
                    })
            });
        });
    }

	/**
	 * Валидирует данные для экшона addVersionFromCopy
	 *
	 * @param {Object}              data                       			Объект с данными запроса
	 * @param {Object}              data.document              			Объект с данными о документе
	 * @param {String}              data.document.id           			Идентификатор копируемого документа
	 * @param {Number}              [data.document.versionNumber]       Номер мажорной версии документа (опционально)
	 * @param {Number}              [data.document.subversionNumber]    Номер минорной версии документа (опционально)
	 * @param {String}              [data.document.language]   			Язык документа (опционально)
	 * @param {Object}              data.user                  			Объект с данными о пользователе
	 * @param {Number}              data.user.id               			Идентификатор пользователя
	 * @returns {Promise<Object>}
	 */
	validateAddVersionFromCopy(data) {
		return new Promise((resolve, reject) => {
			let currentVersion = null;
			this._retrieveCurrentVersion(data).then(result => {
				currentVersion = result;
				return this._retrieveCurrentSubversion(data, currentVersion);
			}).then(currentSubversion => {
				return Joi.object().keys({
					user: this.userSchema.required(),
					document: Joi.object().keys({
						id: Joi.number().integer().positive().required(),
						versionNumber: Joi.number().integer().positive().default(currentVersion),
						subversionNumber: Joi.number().integer().positive().default(currentSubversion),
						language: Joi.string().length(2).default('ru')
					}).required()
				}).required();
			}).then(dataSchema => {
				return Joi.validateAsync(data, dataSchema, config.validation);
			}).then(validData => {
				resolve(validData);
			}).catch(err => {
				reject(new Exception('validation failed', __filename, err));
			});
		});
	}

    /**
     * Валидирует данные для экшона addSubversion
     *
     * @param {Object}              data                            Объект с данными запроса
     * @param {Object}              data.document                   Объект с данными о документе
     * @param {String}              data.document.id                Идентификатор документа
     * @param {UploadableFile}      data.document.file              Объект с данными о загружаемом файле документа
     * @param {Number}              [data.document.versionNumber]   Номер мажорной версии документа (опционально)
     * @param {String}              [data.document.language]        Язык документа (опционально)
     * @param {Object}              data.user                       Объект с данными о пользователе
     * @param {Number}              data.user.id                    Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateAddSubversion(data) {
        return new Promise((resolve, reject) => {
            let validData = null;
            this._retrieveCurrentVersion(data).then(currentVersion => {
                return Joi.object().keys({
                    user: this.userSchema.required(),
                    document: Joi.object().keys({
                        id: Joi.number().integer().positive().required(),
                        file: this.uploadFileSchema.required(),
                        versionNumber: Joi.number().integer().positive().default(currentVersion),
                        language: Joi.string().length(2).default('ru')
                    }).required()
                }).required();
            }).then(dataSchema => {
                return Joi.validateAsync(data, dataSchema, config.validation);
            }).then(validated => {
                validData = validated;
                return super._validateFileAdditional(validated.document.file);
            }).then(() => {
                resolve(validData);
            }).catch(err => {
                reject(new Exception('validation failed', __filename, err));
            });
        });
    }

    /**
     * Валидирует данные для экшона addLanguage
     *
     * @param {Object}              data                                Объект с данными запроса
     * @param {Object}              data.document                       Объект с данными о документе
     * @param {String}              data.document.id                    Идентификатор документа
     * @param {UploadableFile}      data.document.file                  Объект с данными о загружаемом файле документа
     * @param {Number}              [data.document.versionNumber]       Номер мажорной версии документа (опционально)
     * @param {Number}              [data.document.subversionNumber]    Номер минорной версии документа (опционально)
     * @param {String}              data.document.language              Язык документа (обязательно)
     * @param {Object}              data.user                           Объект с данными о пользователе
     * @param {Number}              data.user.id                        Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateAddLanguage(data) {
        return new Promise((resolve, reject) => {
            let validData = null;
            let currentVersion = null;
            this._retrieveCurrentVersion(data).then(result => {
                currentVersion = result;
                return this._retrieveCurrentSubversion(data, currentVersion);
            }).then(currentSubversion => {
                return Joi.object().keys({
                    user: this.userSchema.required(),
                    document: Joi.object().keys({
                        id: Joi.number().integer().positive().required(),
                        file: this.uploadFileSchema.required(),
                        versionNumber: Joi.number().integer().positive().default(currentVersion),
                        subversionNumber: Joi.number().integer().positive().default(currentSubversion),
                        language: Joi.string().length(2).required()
                    }).required()
                }).required();
            }).then(dataSchema => {
                return Joi.validateAsync(data, dataSchema, config.validation);
            }).then(validated => {
                validData = validated;
                return super._validateFileAdditional(validated.document.file);
            }).then(() => {
                resolve(validData);
            }).catch(err => {
                reject(new Exception('validation failed', __filename, err));
            });
        });
    }

    /**
     * Валидирует данные для экшона sign
     *
     * @param {Object}              data                                Объект с данными запроса
     * @param {Object}              data.document                       Объект с данными о документе
     * @param {String}              data.document.id                    Идентификатор документа
     * @param {Number}              [data.document.versionNumber]       Номер мажорной версии документа (опционально)
     * @param {Number}              [data.document.subversionNumber]    Номер минорной версии документа (опционально)
     * @param {String}              [data.document.language]            Язык документа (опционально)
     * @param {UploadableFile}      data.document.signature             Подпись хэша файла документа
     * @param {Object}              data.user                           Объект с данными о пользователе
     * @param {Number}              data.user.id                        Идентификатор пользователя
     * @param {Number}              data.user.orgId                     Идентификатор организации пользователя
     * @returns {Promise<Object>}
     */
    validateSign(data) {
        return new Promise((resolve, reject) => {
            let currentVersion = null;
            this._retrieveCurrentVersion(data).then(result => {
                currentVersion = result;
                return this._retrieveCurrentSubversion(data, currentVersion);
            }).then(currentSubversion => {
                return Joi.object().keys({
                    user: this.userSchema.keys({
                        orgId: Joi.number().integer().positive().required()
                    }).required(),
                    document: Joi.object().keys({
                        id: Joi.number().integer().positive().required(),
                        versionNumber: Joi.number().integer().positive().default(currentVersion),
                        subversionNumber: Joi.number().integer().positive().default(currentSubversion),
                        language: Joi.string().length(2).default('ru'),
                        signature: Joi.string().base64().required()
                    }).required()
                }).required();
            }).then(dataSchema => {
                return Joi.validateAsync(data, dataSchema, config.validation);
            }).then(validated => {
                resolve(validated);
            }).catch(err => {
                reject(new Exception('validation failed', __filename, err));
            });
        });
    }

    /**
     * Валидирует данные для экшона getInfo
     *
     * @param {Object}              data                                Объект с данными запроса
     * @param {Object}              data.document                       Объект с данными о документе
     * @param {Number}              data.document.id                    Идентификатор документа
     * @param {Number}              [data.document.versionNumber]       Номер мажорной версии документа (опционально)
     * @param {Number}              [data.document.subversionNumber]    Номер минорной версии документа (опционально)
     * @param {String}              [data.document.language]            Язык документа (опционально)
     * @returns {Promise<Object>}
     */
    validateGetInfo(data) {
        return new Promise((resolve, reject) => {
            let currentVersion = null;
            this._retrieveCurrentVersion(data).then(result => {
                currentVersion = result;
                return this._retrieveCurrentSubversion(data, currentVersion);
            }).then(currentSubversion => {
                return Joi.object().keys({
                    document: Joi.object().keys({
                        id: Joi.array().items(Joi.number().integer().positive().required()).single().unique().required(),
                        versionNumber: Joi.number().integer().positive().default(currentVersion),
                        subversionNumber: Joi.number().integer().positive().default(currentSubversion),
                        language: Joi.string().length(2).default('ru')
                    })
                });
            }).then(dataSchema => {
                return Joi.validateAsync(data, dataSchema, config.validation);
            }).then(validated => {
                resolve(validated);
            }).catch(err => {
                reject(new Exception('validation failed', __filename, err));
            });
        });
    }

	/**
	 * Валидирует данные для экшона createFromFile
	 *
	 * @param {Object}              data                            Объект с данными запроса
	 * @param {Object}              data.document                   Объект с данными о документе
	 * @param {Object}      		data.document.file              Объект с данными о загружаемом файле документа
	 * @param {Number}				data.document.file.id			Идентификатор файла
	 * @param {String}              data.document.type              Тип документа
	 * @param {String}              [data.document.language]        Язык документа (опционально)
	 * @param {Object}              data.user                       Объект с данными о пользователе
	 * @param {Number}              data.user.id                    Идентификатор пользователя
	 * @returns {Promise<Object>}
	 */
	validateCreateFromFile(data) {
		return new Promise((resolve, reject) => {
			let dataSchema = Joi.object().keys({
				user: this.userSchema.required(),
				document: Joi.object().keys({
					type: Joi.string().max(100).required(),
					language: Joi.string().length(2).default('ru'),
					file: Joi.object().keys({
						id: Joi.number().integer().positive().required()
					}).required()
				}).required(),
			}).required();

			Joi.validate(data, dataSchema, config.validation, (err, value) => {
				if (err)
					return reject(new Exception('validation failed', __filename, err, 'error'));

				return resolve(value);
			});
		});
	}

    /**
     *
     * @param data
     */
    validateGetByType(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                document: Joi.object().keys({
                    type: Joi.string().required()
                }).required()
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err));

                return resolve(value);
            });
        });
    }

    _retrieveCurrentVersion(context) {
        return db.models.SchemaDocumentVersion.max('version_number', {
            where: {
                doc_id: context.document.id
            }
        }).catch(err => {
            throw new Exception('could not retrieve document current version', __filename, err);
        }).then(currentVersion => {
            if (isNaN(currentVersion))
                throw new Exception('could not retrieve document current version', __filename, context.document);
            return currentVersion;
        });
    }

    _retrieveCurrentSubversion(context, versionNumber = context.document.versionNumber) {
        return db.models.SchemaDocumentVersion.max('subversion_number', {
            where: {
                doc_id: context.document.id,
                version_number: versionNumber
            }
        }).catch(err => {
            throw new Exception('could not retrieve document current subversion', __filename, err);
        }).then(currentSubversion => {
            if (isNaN(currentSubversion))
                throw new Exception('could not retrieve document current subversion', __filename, context.document);
            return currentSubversion;
        });
    }
}

module.exports = new DocumentValidator();