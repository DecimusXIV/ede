'use strict';

const path = require('path');
const cryptopro = require('cryptopro');
const mime = require('mime-types');
const Joi = require('joi');
const Promise = require('bluebird');
const readChunk = require('read-chunk');
const imageType = require('image-type');

const config = require('../../config.js');
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));
const FileValidator = require(path.join(config.application.root, config.application.validators, 'file')).FileValidator;

class ImageValidator extends FileValidator {
    constructor() {
        super();

        this.uploadFileSchema = this.uploadFileSchema.keys({
            dimensions: Joi.array().items(
                Joi.object().keys({
                    width: Joi.number().integer().positive().required(),
                    height: Joi.number().integer().positive().required()
                }).required()
            ).single()
        });
    }

    /**
     * @typedef {UploadableFile} UploadableImage
     * @property {Array<Number>[]}  dimensions      Размеры изображения, которые нужно сохранить (ширина, высота)
     */

    /**
     * Валидирует данные для экшона upload
     *
     * @param {Object}                              data            Объект с данными о файле(ах) и пользователе
     * @param {UploadableImage|UploadableImage[]}   data.file       Объект (массив из них) с данными о загружаемом файле
     * @param {Object}                              data.user       Объект с данными о пользователе
     * @param {Number}                              data.user.id    Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateUpload(data) {
        return new Promise((resolve, reject) => {
            let validData = null;
            super.validateUpload(data)
                .then(validated => {
                    validData = validated;
                    return Promise.each(validated.file, file => {
                        return readChunk(path.join(config.application.temp_upload_path, file.tmpName), 0, 12)
                            .catch(err => {
                                throw new Exception('could not detect image format', __filename, err);
                            })
                            .then(buffer => {
                                if (!['jpg', 'png', 'bmp'].includes(imageType(buffer).ext))
                                    throw new Exception('invalid image format', __filename, data.file);
                            });
                    });
                }).then(() => {
                    resolve(validData);
                }).catch(err => {
                    reject(new Exception('validation failed', __filename, err));
                });
        });
    }

    /**
     * Валидирует данные для экшона getResized
     *
     * @param {Object}                  data                     Объект с данными запроса
     * @param {Array<Object>|Object}    data.image               Объект с идентификатором файла и размерами или массив таковых
     * @param {Number}                  data.image.id            Идентификатор файла изображения
     * @param {Array<Object>|Object}    data.image.dimensions    Объект размеров изображения или массив таковых
     * @returns {Promise<Array>}
     */
    validateGetResized(data) {
        return new Promise((resolve, reject) => {
            let dataSchema = Joi.object().keys({
                image: Joi.array().items(
                    Joi.object().keys({
                        id: Joi.number().integer().positive().required(),
                        dimensions: Joi.array().items(
                            Joi.object().keys({
                                width: Joi.number().integer().positive().required(),
                                height: Joi.number().integer().positive().required()
                            }).required()
                        ).single().required()
                    }).required()
                ).single().required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err));

                return resolve(value);
            });
        });
    }
}

module.exports = new ImageValidator();