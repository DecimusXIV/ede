'use strict';

const path = require('path');
const crypto = require('crypto');
const cryptopro = require('cryptopro');
const mime = require('mime-types');
const Joi = require('joi');
const Promise = require('bluebird');
const mmm = require('mmmagic');
const Magic = mmm.Magic;

const config = require('../../config.js');
const Exception = require(path.join(config.application.root, config.application.components, 'exceptions'));

class FileValidator {
    constructor() {
        this.userSchema = Joi.object().keys({
            id: Joi.number().integer().positive().required()
        });

        this.uploadFileSchema = Joi.object().keys({
            name: Joi.string().min(1).max(255).required(),
            tmpName: Joi.string().min(1).max(255).required(),
            module: Joi.string().min(1).max(10).alphanum().required(),
            size: Joi.number().integer().positive().required(),
            type: Joi.string().min(5).max(100).required(),
            hash: Joi.string().hex().required()
        });
    }

    /**
     * @typedef {Object} UploadableFile
     * @property {String}  name      Оригинальное имя файла
     * @property {String}  tmpName   Имя временного файла
     * @property {String}  module    Имя модуля (сервиса), к которому относится файл
     * @property {Number}  size      Размер файла в байтах
     * @property {String}  type      MIME тип файла
     * @property {String}  hash      Хэш файла в шестнадцатиричном формате, вычисленный по алгоритму ГОСТ 34.11-94
     */

    /**
     * Валидирует данные для экшонов upload/uploadAsync
     *
     * @param {Object}                           data               Объект с данными о файле(ах) и пользователе
     * @param {UploadableFile|UploadableFile[]}  data.file          Объект (массив из них) с данными о загружаемом файле
     * @param {Object}                           data.user          Объект с данными о пользователе
     * @param {Number}                           data.user.id       Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateUpload(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                user: this.userSchema.required(),
                file: Joi.array().items(this.uploadFileSchema.required()).single().required()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                Promise.each(value.file, file => {
                    return this._validateFileAdditional(file).catch(err => {
                        return reject(new Exception('validation failed', __filename, err, err.type));
                    });
                }).then(() => {
                    return resolve(value);
                });
            });
        });
    }

    /**
     * Валидирует данные для экшона getInfo
     *
     * @param {Object}              data          Объект с данными запроса
     * @param {Object}              data.file     Объект с данными о файле
     * @param {Number|Number[]}     data.file.id  Идентификатор(ы) файла(ов)
     * @returns {Promise<Object>}
     */
    validateGetInfo(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.array().items(Joi.number().integer().positive().required()).single().unique().required()
                })
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона delete
     *
     * @param {Object}                  data          Объект с данными запроса
     * @param {Object}                  data.file     Объект с данными о файле
     * @param {Array<Number>|Number}    data.file.id  Идентификатор файла
     * @param {Object}                  data.user     Объект с данными о пользователе
     * @param {Number}                  data.user.id  Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateDelete(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.array().items(
                        Joi.number().integer().positive().required()
                    ).single().unique().required()
                }).required(),
                user: this.userSchema.required(),
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона sign
     *
     * @param {Object}  data                Объект с данными запроса
     * @param {Object}  data.file           Объект с данными о файле
     * @param {Number}  data.file.id        Идентификатор файла
     * @param {String}  data.file.signature Подпись хэша файла
     * @param {Object}  data.user           Объект с данными о пользователе
     * @param {Number}  data.user.id        Идентификатор пользователя
     * @param {Number}  data.user.orgId    Идентификатор организации пользователя
     * @returns {Promise<Object>}
     */
    validateSign(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.number().integer().positive().required(),
                    signature: Joi.string().base64().required()
                }),
                user: Joi.object().keys({
                    id: Joi.number().integer().positive().required(),
                    orgId: Joi.number().integer().positive().required()
                }),
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона download
     *
     * @param {Object}  data        Объект с данными запроса
     * @param {Number}  data.id     Идентификатор файла
     * @param {String}  data.hash   Хэш для проверки валидности запроса
     * @returns {Promise<Object>}
     */
    validateDownload(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                id: Joi.number().integer().positive().required(),
                hash: Joi.any()
                    .equal(crypto.createHash('md5').update(data.id.toString() + config.settings.salt).digest('hex'))
                    .error(() => 'hash is invalid')
                    .required().strip()
            });

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона computeHash
     *
     * @param   {Object} data               Объект с данными запроса
     * @param   {Object} data.file          Объект с данными о файле
     * @param   {String} data.file.path     Путь к файлу, хэш которого требуется вычислить
     * @returns {Promise<Object>}
     */
    validateComputeHash(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    path: Joi.string().required()
                }).required()
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона verifySignature
     *
     * @param {Object}  data                    Объект с данными запроса
     * @param {Object}  data.file               Объект с данными о файле
     * @param {Number}  data.file.id            Идентификатор файла
     * @param {String}  data.file.signature     Проверяемая подпись
     * @param {Object}  data.user               Оъект с данными пользователя
     * @param {Number}  data.user.id            Идентификатор пользователя
     * @returns {Promise<Object>}
     */
    validateVerifySignature(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                file: Joi.object().keys({
                    id: Joi.number().integer().positive().required(),
                    signature: Joi.string().base64().required()
                }).required(),
                user: Joi.object().keys({
                    id: Joi.number().integer().positive().required()
                }).required()
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        });
    }

    /**
     * Валидирует данные для экшона downloadArchive
     *
     * @param {Object}  data                Объект с данными запроса
     * @param {Number}  data.id             Идентификатор файла
     * @param {String}  data.hash           Хэш для проверки валидности запроса
     * @param {Number}  [data.announceID]   Идентификатор извещения, к которому прикреплен файл
     * @param {Number}  [data.offerID]      Идентификатор заявки, к которой прикреплен файл
     * @param {Number}  [data.contractID]   Идентификатор договора, к которому прикреплен файл
     * @param {Number}  [data.lotID]        Идентификатор лота, к которому прикреплен файл
     * @param {String}  [data.type]         Тип архива документов
     * @returns {Promise<Object>}
     */
    validateDownloadArchive(data) {
        return new Promise((resolve, reject) => {
            const dataSchema = Joi.object().keys({
                id: Joi.number().integer().positive().required(),
                hash: Joi.any()
                    .equal(crypto.createHash('md5').update(data.id.toString() + config.settings.salt).digest('hex'))
                    .error(() => 'hash is invalid')
                    .required().strip(),
                announceID: Joi.number().integer().positive(),
                offerID: Joi.number().integer().positive(),
                contractID: Joi.number().integer().positive(),
                lotID: Joi.number().integer().positive(),
                type: Joi.string().alphanum().uppercase().length(2).default('default')
            }).required();

            Joi.validate(data, dataSchema, config.validation, (err, value) => {
                if (err)
                    return reject(new Exception('validation failed', __filename, err, 'error'));

                return resolve(value);
            });
        })
    }

    /**
     * Дополнительная валидация для загружаемого файла
     *
     * @param {UploadableFile}  file    Объект с данными о загружаемом файле
     * @returns {Promise}
     */
    _validateFileAdditional(file) {
        return new Promise((resolve, reject) => {
            let tmpPath = path.join(config.application.temp_upload_path, file.tmpName);
            // TODO: можно еще дополнительно проверять тип на корректность вот так, а нужно ли?
            // FIXME: возникают проблемы с тем, что php и magic немного по разному определяют mime
            /*let magic = Promise.promisifyAll(new Magic(mmm.MAGIC_MIME_TYPE));

            magic.detectFileAsync(tmpPath).catch(err => {
                reject(new Exception('could not determine file type', __filename, err));
            }).then(type => {
                if (type !== file.type)
                    return reject(new Exception('invalid file type', __filename, file));
                else
                    return true;
            }).then(() => {*/
            cryptopro.verifyHash(tmpPath, file.hash).then(() => {
                resolve();
            }).catch(err => {
                if (err.code && err.code === 148)
                    reject(new Exception('invalid file hash', __filename, err, 'error'));
                else
                    reject(new Exception('error while validating file hash', __filename, err, 'system'));
            });
        });
    }
}

module.exports.fileValidator = new FileValidator();
module.exports.FileValidator = FileValidator;