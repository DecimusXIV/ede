'use strict';

const TCPServer = require('./app/server/tcp.js');
const HTTPServer = require('./app/server/http.js');

new TCPServer().start();
new HTTPServer().start();